/*
===============================================================================
 Name        : PuestaEnMarchaKit.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : main definition
===============================================================================
*/

//#include <cr_section_macros.h>	//Comentado 14-02-2016
#include "KitInfo2.h"

volatile uint32_t Ticks;
volatile uint32_t Demora_LCD;
volatile uint8_t flagPWMrojo, flagPWMverde, flagPWMazul;
volatile int32_t bufferLedRGBrojo, bufferLedRGBverde, bufferLedRGBazul;
volatile uint8_t flagLedsRGB;
uint8_t BufferLeds;							//!< Variable de estado de las LEDS
volatile uint8_t BufferEntradas;
volatile uint8_t BufferSalidas;				//!< Buffer de Salidas Relay
volatile uint8_t decimas;						//!< Base de tiempo decimas
volatile uint8_t segundos;
uint8_t flagTiempoAuxiliar, flagSeno, indiceSeno;
volatile uint8_t Buffer_LCD[TOPE_BUFFER_LCD];
volatile uint32_t inxInLCD;
volatile uint32_t inxOutLCD;
volatile uint32_t cantidadColaLCD;
volatile TIEMPOS TmrRun[TIMERS] ;					//!< Buffer de Temporizadores
volatile EVENTOS Evento[TIMERS] ;					//!< Buffer de eventos vencidos
uint8_t flagTitileo;
uint8_t flagRefrescoADC;
volatile uint8_t key;
volatile uint8_t keyFija;
uint32_t buffer_seno[ SENO_MUESTRAS ];
uint32_t valorADC[ 3 ];
uint32_t medida[ CANTcANALES ][  MUESTRAS];
uint8_t estado;
uint8_t cont;
struct matriz EX4_Caracteres[16][16];
volatile struct matriz EX4_BufferDisplay[ EX4_DIGITOS ];
volatile struct matriz EX4_BufferMatriz[ EX4_BUFFER ];
int32_t EX4_lstring;
volatile uint8_t EX4_modo;
// Buffer de Transmision
volatile uint8_t BufferTx[TXBUFFER_SIZE];
// Buffer de Recepcion
volatile uint8_t BufferRx[RXBUFFER_SIZE];

// Indices de Transmision
volatile uint8_t IndiceTxIn,IndiceTxOut;

// Indices de Recepcion
volatile uint8_t IndiceRxIn,IndiceRxOut;
volatile uint8_t TxStart;
volatile int UPrgAuxiliar[16];
uint8_t CantidadCaracteresTrama;
char ValidarTrama[CANTtRAMAvALIDAR];
unsigned char teclaPwm = NO_KEY;

//I2C
volatile uint32_t I2CMasterState = I2C_IDLE;
volatile uint32_t I2CSlaveState = I2C_IDLE;

volatile uint32_t I2CCmd;
volatile uint32_t I2CMode;

volatile uint8_t I2CMasterBuffer[BUFSIZE];
volatile uint8_t I2CSlaveBuffer[BUFSIZE];
volatile uint32_t I2CCount = 0;
volatile uint32_t I2CReadLength;
volatile uint32_t I2CWriteLength;

volatile uint32_t RdIndex = 0;
volatile uint32_t WrIndex = 0;

//ADC
volatile uint8_t flagADCext, flagADCpote, flagADCterm;

int main(void) {


    Inicializar( );
    Display_lcd( "Prueba Kit V2.0 " , 0 , 0 ) ;
    Display_lcd( " UTN.BA Info II " , 1 , 0 ) ;

    while(1) {

    	Maquina();
    	//Cada 100useg cambio el valor de la salida del buffer:
		if( flagSeno )
		{
			SETDAC(buffer_seno[indiceSeno]);
			indiceSeno++;
			indiceSeno %= SENO_MUESTRAS;
		}
		else
			SETDAC(MIN_VALOR_DAC);


    }
    return 0 ;
}
