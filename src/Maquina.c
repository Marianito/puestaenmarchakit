/**
 	\file main.c
 	\brief Programa de prueba de funciones de Infotronic
 	\details Placa base + Expansion 3
 	\author Ing. Marcelo Trujillo
 	\date 2012.05.31
*/
#include "KitInfo2.h"



extern unsigned char estado;
extern volatile unsigned char BufferEntradas;
extern unsigned char flagTitileo;
extern unsigned char cont;
extern unsigned int valorADC[];
extern unsigned char flagRefrescoADC;
extern unsigned char flagTiempoAuxiliar, indiceSeno, flagSeno;
extern unsigned int buffer_seno[ ];
extern volatile int bufferLedRGBrojo;
extern volatile int bufferLedRGBverde;
extern volatile int bufferLedRGBazul;
extern volatile unsigned char flagLedsRGB;
extern unsigned char teclaPwm;
uint8_t teclaAdc;
extern volatile unsigned char decimas;
extern volatile unsigned char segundos;
extern volatile uint8_t flagADCext, flagADCpote, flagADCterm;

void Maquina (void)
{

	unsigned char teclita = NO_KEY, teclaFija;
	char texto[8];
	static unsigned char funcionPrueba = IDLE;

	conversor();
	teclita = Teclado( ) ;
	teclaPwm = teclita;
	if( teclita != NO_KEY)
		teclaAdc = teclita;

	EX4_Prueba ( teclita );
			if( teclita == 0 )
				BEEPER = ON;
			if( teclita == 3 )
				BEEPER = OFF;

	teclaFija = TecladoFijo();

	if( teclaFija != NO_KEY )
		{
		switch( teclaFija )
				{
				case 4:
					sprintf( texto, "fun1");
					EX4_Display( texto, 8);
					funcionPrueba = RELESeNTRADASaDC;
					TimerStart( TIMERlEDS , TIEMPOtITILEO );
					TimerStart( TIMERrEFRESCOaDC , TIEMPOrEFRESCOaDC );
					Display_lcd( "RELES ENTR ADC" , 1 , 0 ) ;
					//Apaga los tres timers
					T0_TCR_CE = 0;					// Apago y reseteo el temporizador
					T0_TCR_CR = 1;

					T1_TCR_CE = 0;					// Apago y reseteo el temporizador
					T1_TCR_CR = 1;

					T2_TCR_CE = 0;					// Apago y reseteo el temporizador
					T2_TCR_CR = 1;
					bufferLedRGBrojo = 0;
					bufferLedRGBverde = 0;
					bufferLedRGBazul = 0;
					flagLedsRGB = 0;
					break;
				case 1:
					sprintf( texto, "fun2") ;
					EX4_Display( texto, 8);
					funcionPrueba = UART0y1;
					Display_lcd( "Prueba Kit V2.0 " , 0 , 0 ) ;
					Display_lcd( "  UART0 y UART1 " , 1 , 0 ) ;
					apagar();
					break;
				case 0:
					EX4_Display( "IDLE", 8);
					Display_lcd( "      IDLE      " , 1 , 0 ) ;
					Display_lcd( "Prueba Kit V2.0 " , 0 , 0 ) ;
					funcionPrueba = IDLE;
					break;
				case 3:
					sprintf( texto, "fun4") ;
					EX4_Display( texto, 8);
					funcionPrueba = PWM;
					Display_lcd( "Prueba Kit V2.0 " , 0 , 0 ) ;
					Display_lcd( "  PWM y EEPROM  " , 1 , 0 ) ;
					apagar();

					//Disparar los timers
					T0_TCR_CE = 0;					// Apago y reseteo el temporizador0
					T0_TCR_CR = 1;

					T0_TCR_CR = 0;					// Enciendo el temporizador0
					T0_TCR_CE = 1;

					T1_TCR_CE = 0;					// Apago y reseteo el temporizador1
					T1_TCR_CR = 1;

					T1_TCR_CR = 0;					// Enciendo el temporizador1
					T1_TCR_CE = 1;

					T2_TCR_CE = 0;					// Apago y reseteo el temporizador2
					T2_TCR_CR = 1;

					T2_TCR_CR = 0;					// Enciendo el temporizador2
					T2_TCR_CE = 1;

					break;
				case 2:
					sprintf( texto, "fun3") ;
					EX4_Display( texto, 8);
					Display_lcd( "      DAC       " , 1 , 0 ) ;
					apagar();
					funcionPrueba = DAC;
					break;

				default:
					break;
				}
		}

	switch( funcionPrueba )
	{
	case IDLE:
		apagar();
		break;

	case RELESeNTRADASaDC:
		ProbarRelesEntradasADC();
		break;

	case UART0y1:
		ProbarUart0y1();
		break;

	case PWM:
		pwm();
		break;

	case DAC:
		dac();
		break;

	default:
		break;
	}
}

void ProbarUart0y1(void)
{
	unsigned int enviarUart0 = 0;
	unsigned char datoUart0 = 0;
	char texto[16];

	if ( U0LSR & 0x01 ) //Llego un dato nuevo a la UART0:
			{

				datoUart0 = U0RBR;
				sprintf( texto, "Dato en UART0: %c", datoUart0) ;
				Display_lcd( texto, 0 , 0 );
				enviarUart0 = 1;
			}
	if ( enviarUart0 == 1 )
	{

		if ( U0LSR & 0x20 ) //Si esta vacio el THR
		{
			//U0THR = cadena[i++];
			U0THR = datoUart0;
			enviarUart0 = 0;
			/*if ( i == TAM )
			{
				i = 0;
				enviar = 0;
			}*/
		}
	}



	AnalizarTramaUart1();


}

void pwm( void )
{
	static unsigned char cambiarColor = 0;
	static float rojo = 0;
	static float verde = 0;
	static float azul = 0;
	char texto[17];
	volatile uint8_t lecturaEeprom, i;
	static uint8_t escribirEeprom = 0;
	static uint16_t dirEeprom;

	flagLedsRGB = 1;

	switch( teclaPwm )
	{

	case 4:
	case 1:
		escribirEeprom = (int)(rojo + verde + azul);
		if( escribirEeprom )
			dirEeprom = (int)(rojo*111 + verde*325 + azul*197);
		else
		{
			i = decimas*3 + segundos*2;
			i %= SENO_MUESTRAS;
			dirEeprom = buffer_seno[ i ];
		}

		escribirEeprom %= 0xFF;		//Forzarlo a valores de byte
		dirEeprom %= 0x7FFF;		//Forzarlo a rango de direcciones de 24LC256
		EX_EEPROM_WRITE(dirEeprom, escribirEeprom);
		sprintf( texto, "EepromDir %d     ", dirEeprom);
		Display_lcd( texto , 0 , 0 ) ;
		sprintf( texto, "Eeprom val %d    ", escribirEeprom);
		Display_lcd( texto , 1 , 0 ) ;
	break;

	case 5:
		sprintf( texto, " Leyendo Eeprom ");
		Display_lcd( texto , 0 , 0 ) ;
		lecturaEeprom = EX_EEPROM_READ(dirEeprom);

		if( lecturaEeprom == escribirEeprom )
		{
			sprintf( texto, "EepromDir %d     ", dirEeprom);
			Display_lcd( texto , 0 , 0 ) ;
			sprintf( texto, "Eeprom OK %d    ", lecturaEeprom);
			Display_lcd( texto , 1 , 0 ) ;

		}
		else
		{
			sprintf( texto, "EepromDir %d     ", dirEeprom);
			Display_lcd( texto , 0 , 0 ) ;
			sprintf( texto, "Eeprom NOK %d    ", lecturaEeprom);
			Display_lcd( texto , 1 , 0 ) ;
		}
	break;

	case 6:
		cambiarColor ++;
		cambiarColor %= 3;

		switch(cambiarColor)
		{
		case 0:
		sprintf( texto, "Verde ") ;
		Display_lcd( texto , 1 ,10 ) ;
		break;

		case 1:
		sprintf( texto, "Azul  ") ;
		Display_lcd( texto , 1 ,10 ) ;
		break;

		case 2:
		sprintf( texto, "Rojo   ") ;
		Display_lcd( texto , 1 ,10 ) ;
		break;

		default:
		break;

		}
		//sprintf( texto, "%d", cambiarColor) ;
		//Display_lcd( texto , 1 ,10 ) ;
	break;

	case 7:
	case 0x87:
		if( cambiarColor == AZUL )
			{
			if( teclaPwm == 7 )
				bufferLedRGBazul -= PASOSrEPETICION;
			else
				bufferLedRGBazul --;
			}
		if( cambiarColor == VERDE )
			{
			if( teclaPwm == 7 )
				bufferLedRGBverde -= PASOSrEPETICION;
			else
				bufferLedRGBverde --;
			}
		if( cambiarColor == ROJO )
			{
			if( teclaPwm == 7 )
				bufferLedRGBrojo -= PASOSrEPETICION;
			else
				bufferLedRGBrojo --;
			}


		rojo = (float)(bufferLedRGBrojo*100 /(FULLpWM));
		verde = (float)(bufferLedRGBverde*100)/(FULLpWM);
		azul = (float)(bufferLedRGBazul*100)/( FULLpWM);

		if( rojo < 0 )
			rojo = 0;

		if( verde < 0 )
			verde = 0;

		if( azul < 0 )
			azul = 0;

		if( rojo > 99 )
			rojo = 99;

		if( verde > 99 )
			verde = 99;

		if( azul > 99 )
			azul = 99;

		sprintf( texto, " %2.1f %2.1f %2.1f ", rojo, verde, azul);
		Display_lcd( texto , 0 , 0 ) ;
		break;

	case 8:
	case 0x88:
		if( cambiarColor == AZUL )
			{
			if( teclaPwm == 8 )
				bufferLedRGBazul += PASOSrEPETICION;
			else
				bufferLedRGBazul ++;
			}
		if( cambiarColor == VERDE )
			{
			if( teclaPwm == 8 )
				bufferLedRGBverde += PASOSrEPETICION;
			else
				bufferLedRGBverde ++;
			}
		if( cambiarColor == ROJO )
			{
			if( teclaPwm == 8 )
				bufferLedRGBrojo += PASOSrEPETICION;
			else
				bufferLedRGBrojo ++;
			}


		rojo = (float)(bufferLedRGBrojo*100 /(FULLpWM));
		verde = (float)(bufferLedRGBverde*100)/(FULLpWM);
		azul = (float)(bufferLedRGBazul*100)/( FULLpWM);

		if( rojo < 0 )
			rojo = 0;

		if( verde < 0 )
			verde = 0;

		if( azul < 0 )
			azul = 0;

		if( rojo > 99 )
			rojo = 99;

		if( verde > 99 )
			verde = 99;

		if( azul > 99 )
			azul = 99;

		sprintf( texto, " %2.1f %2.1f %2.1f ", rojo, verde, azul) ;
		Display_lcd( texto , 0 , 0 ) ;
		break;


	default:
		break;

	}

	if( bufferLedRGBrojo <= ( MINpWM + DELTApWMmIN + 1 ) )
		{
		bufferLedRGBrojo = MINpWM + DELTApWMmIN;
		}

	if( bufferLedRGBrojo >= ( FULLpWM - DELTApWMmAX))
		{
		bufferLedRGBrojo = ( FULLpWM -  DELTApWMmAX );
		}


	if( bufferLedRGBverde <= ( MINpWM + DELTApWMmIN + 1 ) )
		{
		bufferLedRGBverde = MINpWM + DELTApWMmIN;
		}

	if( bufferLedRGBverde >= ( FULLpWM - DELTApWMmAX))
		{
		bufferLedRGBverde = ( FULLpWM -  DELTApWMmAX );
		}


	if( bufferLedRGBazul <= ( MINpWM + DELTApWMmIN + 1 ) )
		{
		bufferLedRGBazul = MINpWM + DELTApWMmIN;
		}

	if( bufferLedRGBazul >= ( FULLpWM - DELTApWMmAX))
		{
		bufferLedRGBazul = ( FULLpWM -  DELTApWMmAX );
		}
}
void dac(void)
{
	unsigned int lecturaADC, primerDigitoMiliVolt, decimalesMiliVolts, aux;
	unsigned char  c, flagDAC;
	char texto[8];

	c = Teclado( ) ;

	switch( c )
	{
	case 1:
		flagSeno = 0;
		flagDAC = 1;
		if( flagDAC )
		{
			sprintf( texto, "Leer el pote...") ;
			Display_lcd( texto, 0 , 0 );
			//lecturaADC = AD0DR2;		//Externa
			lecturaADC = AD0DR5;	//Pote
			//lecturaADC = AD0DR1;	//Termistor
			if ( ADC_DONE (lecturaADC) )
					{
					lecturaADC = ( lecturaADC >> 4 ) & 0x0000FFFF;
					aux = ConvertirMiliVolts( lecturaADC );
					primerDigitoMiliVolt = aux / 1000;
					aux %=  1000;
					decimalesMiliVolts = aux;
					sprintf( texto, "%d.%d", primerDigitoMiliVolt, decimalesMiliVolts) ;
					EX4_Display( texto, 8);
					SETDAC(lecturaADC);
					}
		}
		break;

	case 4:
		flagSeno = 1;
		flagDAC = 0;

		lecturaADC = buffer_seno[indiceSeno];
		sprintf( texto, "%d", lecturaADC) ;
		EX4_Display( texto, 8);
		sprintf( texto, "Tono Senoidal") ;
		Display_lcd( texto, 0 , 0 );

		break;

	default:
		break;
	}

}

void ProbarRelesEntradasADC( void )
{
	static unsigned char i = 0;
	unsigned char  entradas;
	unsigned int primerDigitoMiliVolt = 0, decimalesMiliVolts = 0, aux;
	char texto[8];

	flagLedsRGB = 0;

	switch(estado)
		{
		case ESTADOcERRADO:

			Relays (RELAY0, OFF);
			Relays (RELAY1, OFF);
			Relays (RELAY2, OFF);
			Relays (RELAY3, ON);
			if( flagTitileo )
			{
				flagTitileo = 0;
				estado = ESTADOaBRIENDO;
				Display_lcd( "RELAY 1 " , 0 , 0 ) ;
				//i ++;
				//i %= CANTcANALES;
			}

		break;

		case ESTADOaBRIENDO:

			Relays (RELAY0, OFF);
			Relays (RELAY1, ON);
			Relays (RELAY2, OFF);
			Relays (RELAY3, OFF);
			if( flagTitileo )
			{
				flagTitileo = 0;
				estado = ESTADOaBIERTO;
				Display_lcd( "RELAY 0 " , 0 , 0 ) ;
			}

		break;

		case ESTADOaBIERTO:

			Relays (RELAY0, ON);
			Relays (RELAY1, OFF);
			Relays (RELAY2, OFF);
			Relays (RELAY3, OFF);
			if( flagTitileo )
			{
				flagTitileo = 0;
				estado = ESTADOcERRANDO;
				Display_lcd( "RELAY 2 " , 0 , 0 ) ;
			}
		break;

		case ESTADOcERRANDO:
			Relays (RELAY0, OFF);
			Relays (RELAY1, OFF);
			Relays (RELAY2, ON);
			Relays (RELAY3, OFF);
			if( flagTitileo )
			{
				flagTitileo = 0;
				estado = ESTADOcERRADO;
				Display_lcd( "RELAY 3 " , 0 , 0 ) ;
			}

		break;

		default:
			if( estado >= ESTADOmAXIMO )
				estado = ESTADOcERRADO;

		break;

		}

		entradas = evualuarSensores();
		if( entradas )
		{
			switch(entradas)
			{
			case 0x01:
				Led (AZUL, ON);
				Led (ROJO, OFF);
				Led (VERDE, OFF);
				sprintf( texto, "Blue") ;
				EX4_Display( texto, 8);
				break;
			case 0x02:
				Led (AZUL, OFF);
				Led (ROJO, ON);
				Led (VERDE, OFF);
				sprintf( texto, "Red ") ;
				EX4_Display( texto, 8);
				break;
			case 0x04:
				Led (AZUL, OFF);
				Led (ROJO, OFF);
				Led (VERDE, ON);
				sprintf( texto, "Gree") ;
				EX4_Display( texto, 8);
				break;
			case 0x03:
				Led (AZUL, ON);
				Led (ROJO, ON);
				Led (VERDE, OFF);
				sprintf( texto, "B+R") ;
				EX4_Display( texto, 8);
				break;
			case 0x05:
				Led (AZUL, OFF);
				Led (ROJO, ON);
				Led (VERDE, ON);
				sprintf( texto, "R+G") ;
				EX4_Display( texto, 8);
				break;
			case 0x06:
				Led (AZUL, ON);
				Led (ROJO, OFF);
				Led (VERDE, ON);
				sprintf( texto, "B+G") ;
				EX4_Display( texto, 8);
				break;
			case 0x07:
				Led (AZUL, ON);
				Led (ROJO, ON);
				Led (VERDE, ON);
				sprintf( texto, "BRG ") ;
				EX4_Display( texto, 8);
				break;
			default:
				break;
			}
		}
		else
			apagarLeds();


		if( flagRefrescoADC )
			{
				flagRefrescoADC = 0;

				switch( teclaAdc )
				{
				case 8:
					i = 0;
					sprintf( texto, "CH1 Term") ;
					Display_lcd( texto , 0 , 8 ) ;
					break;
				case 7:
					i = 1;
					sprintf( texto, "CH2 Ext ") ;
					Display_lcd( texto , 0 , 8 ) ;
					break;
				case 6:
					i = 2;
					sprintf( texto, "CH5 Pote") ;
					Display_lcd( texto , 0 , 8 ) ;
					break;
				default:
					Display_lcd( "        " , 0 , 8 ) ;
					break;

				}
				//valorADC[i] = LeeADC();
				if( valorADC[i] )
				{
				aux = ConvertirMiliVolts( valorADC[i] );
				primerDigitoMiliVolt = aux / 1000;
				aux %=  1000;
				decimalesMiliVolts = aux;
				sprintf( texto, "%d.%d", primerDigitoMiliVolt, decimalesMiliVolts) ;
				EX4_Display( texto, 8);
				}
			}


			/*
			//Probar individualmente cada canal

			primerDigitoMiliVolt = LeeADC();
			sprintf( texto, "%d", primerDigitoMiliVolt) ;
			EX4_Display( texto, 8);
			*/

}

void giro1Motor( void )
{
	Relays (RELAY0, ON);
	Relays (RELAY1, OFF);
}

void giro2Motor( void )
{
	Relays (RELAY1, ON);
	Relays (RELAY0, OFF);
}

void pararMotor( void )
{
	Relays (RELAY0, OFF);
	Relays (RELAY1, OFF);
	Relays (RELAY2, OFF);
	Relays (RELAY3, OFF);
}
unsigned char evualuarSensores ( void )
{
	unsigned char estadoSensores = 0;

	if( Entrada0 == OFF )
		estadoSensores |= 0x01;

	if( Entrada1 == OFF )
		estadoSensores |= 0x02;

	if( Entrada2 == OFF )
		estadoSensores |= 0x04;

	return estadoSensores;
}

void titilar( unsigned char color )
{

	if( flagTitileo )
	{
		Led (color, ON);
	}
	else
		Led (color, OFF);

}

void apagarLeds( void )
{
	Led (AZUL, OFF);
	Led (ROJO, OFF);
	Led (VERDE, OFF);
}

void apagar( void )
{
	pararMotor();
	estado = ESTADOcERRADO;
	apagarLeds();
	TimerStop( TIMERlEDS );
	TimerStop( TIMERrEFRESCOaDC );


	cont = 0;

	//Apaga los tres timers
	T0_TCR_CE = 0;					// Apago y reseteo el temporizador
	T0_TCR_CR = 1;

	T1_TCR_CE = 0;					// Apago y reseteo el temporizador
	T1_TCR_CR = 1;

	T2_TCR_CE = 0;					// Apago y reseteo el temporizador
	T2_TCR_CR = 1;
	bufferLedRGBrojo = 0;
	bufferLedRGBverde = 0;
	bufferLedRGBazul = 0;
	flagLedsRGB = 0;
	flagSeno = 0;
	flagADCterm = 0;
	flagADCext = 0;
	flagADCpote = 0;


}
