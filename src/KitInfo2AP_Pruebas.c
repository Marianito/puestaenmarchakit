/**
 	\file KitInfo2AP_Pruebas.c
 	\brief Aplicacion de prueba de funciones de Infotronic
 	\details Placa base + Expansion 4
 	\author Ing. Marcelo Trujillo
 	\date 2012.04.25
*/
#include "KitInfo2.h"

extern volatile struct matriz EX4_BufferDisplay[EX4_BUFFER];
extern volatile int EX4_lstring;
extern volatile unsigned char EX4_modo;

void EX4_Prueba  ( unsigned char c )
{
	char aux[25] ;
	static char PrimeraVez = 0;

	if ( !PrimeraVez )
	{
		EX4_Display( "UTN.BA INFO II " , 10 );
		PrimeraVez = 1 ;
	}

	if ( c != NO_KEY )
	{
		sprintf(aux,"Tecla: %02x       ",(int)c) ;
		Display_lcd( aux , 1 , 0 ) ;
	}

	switch ( c )
	{
		case F0:
			EX4_Display( NULL , ROTAR_DERECHA );
			break;
		case F1:
			EX4_Display( NULL , ROTAR_IZQUIERDA );
			break;
		case F2:
			EX4_Display( NULL ,ROTAR_ABAJO );
			break;
		case F3:
			EX4_Display( NULL ,ROTAR_ARRIBA );
			break;
		case F4:
			EX4_Display( NULL ,10);
			break;
		case F5:
			EX4_Display( "UTN.BA INFO II " , 10 );
			break;
	}
}
