#ifndef _registros
	#include "registros.h"
	#define		_registros
#endif
/*------------- General Purpose Input/Output (GPIO) --------------------------*/
typedef struct
{
	__RW register_t		IR;
	__RW register_t 	TCR;
	__RW register_t 	TC;
	__RW register_t 	PR;
	__RW register_t 	PC;
	__RW register_t 	MCR;
	__RW register_t 	MR[4];
	__RW register_t 	CCR;
	__R register_t 		CR[2];
	__RW register_t 	EMR;
	__RW register_t 	CTCR;

} timer_t;

//__IO uint32_t	 __attribute__ ((at(0x2009C000UL))) pepe;

#define		TIMER0		( ( timer_t  * ) 0x40004000UL )
#define		TIMER1		( ( timer_t  * ) 0x40008000UL )
#define		TIMER2		( ( timer_t  * ) 0x40090000UL )
#define		TIMER3		( ( timer_t  * ) 0x40094000UL )

//***************** TIMER 0 ********************
/** IR - INTERRUPT REGISTER */
#define		T0_IR			TIMER0->IR.dword

#define		T0_IR_MR0		TIMER0->IR.bit00
#define		T0_IR_MR1		TIMER0->IR.bit01
#define		T0_IR_MR2		TIMER0->IR.bit02
#define		T0_IR_MR3		TIMER0->IR.bit03
#define		T0_IR_CR0		TIMER0->IR.bit04
#define		T0_IR_CR1		TIMER0->IR.bit05

/** TCR - TIMER CONTROL REGISTER */
#define		T0_TCR			TIMER0->TCR.dword

#define		T0_TCR_CE		TIMER0->TCR.bit00
#define		T0_TCR_CR		TIMER0->TCR.bit01

/** CTCR - COUNT CONTROL REGISTER */
#define		T0_CTCR			TIMER0->CTCR.dword

#define		T0_CTCR_TCM		TIMER0->CTCR.bits0y1
#define		T0_CTCR_CIS		TIMER0->CTCR.bit02y3

/** TC - TIMER COUNTER REGISTER */
#define		T0_TC			TIMER0->TC.dword

/** PR - PRESCALE REGISTER */
#define		T0_PR			TIMER0->PR.dword

/** PC - PRESCALE COUNTER REGISTER */
#define		T0_PC			TIMER0->PC.dword

/** MR - MATCH CONTROL REGISTER */
#define		T0_MR0			TIMER0->MR[0].dword
#define		T0_MR1			TIMER0->MR[1].dword
#define		T0_MR2			TIMER0->MR[2].dword
#define		T0_MR3			TIMER0->MR[3].dword

/** MCR - MATCH CONTROL REGISTER */
#define		T0_MCR			TIMER0->MCR.dword

#define		T0_MR0I			TIMER0->MCR.bit00
#define		T0_MR0R			TIMER0->MCR.bit01
#define		T0_MR0S			TIMER0->MCR.bit02

#define		T0_MR1I			TIMER0->MCR.bit03
#define		T0_MR1R			TIMER0->MCR.bit04
#define		T0_MR1S			TIMER0->MCR.bit05

#define		T0_MR2I			TIMER0->MCR.bit06
#define		T0_MR2R			TIMER0->MCR.bit07
#define		T0_MR2S			TIMER0->MCR.bit08

#define		T0_MR3I			TIMER0->MCR.bit09
#define		T0_MR3R			TIMER0->MCR.bit10
#define		T0_MR3S			TIMER0->MCR.bit11

//***************** TIMER 1 ********************
/** IR - INTERRUPT REGISTER */
#define		T1_IR			TIMER1->IR.dword

#define		T1_IR_MR0		TIMER1->IR.bit00
#define		T1_IR_MR1		TIMER1->IR.bit01
#define		T1_IR_MR2		TIMER1->IR.bit02
#define		T1_IR_MR3		TIMER1->IR.bit03
#define		T1_IR_CR0		TIMER1->IR.bit04
#define		T1_IR_CR1		TIMER1->IR.bit05

/** TCR - TIMER CONTROL REGISTER */
#define		T1_TCR			TIMER1->TCR.dword

#define		T1_TCR_CE		TIMER1->TCR.bit00
#define		T1_TCR_CR		TIMER1->TCR.bit01

/** CTCR - COUNT CONTROL REGISTER */
#define		T1_CTCR			TIMER1->CTCR.dword

#define		T1_TCR_TCM		TIMER1->CTCR.bits0y1
#define		T1_TCR_CIS		TIMER1->CTCR.bit02y3

/** TC - TIMER COUNTER REGISTER */
#define		T1_TC			TIMER1->TC.dword

/** PR - PRESCALE REGISTER */
#define		T1_PR			TIMER1->PR.dword

/** PC - PRESCALE COUNTER REGISTER */
#define		T1_PC			TIMER1->PC.dword

/** MR - MATCH CONTROL REGISTER */
#define		T1_MR0			TIMER1->MR[0].dword
#define		T1_MR1			TIMER1->MR[1].dword
#define		T1_MR2			TIMER1->MR[2].dword
#define		T1_MR3			TIMER1->MR[3].dword

/** MCR - MATCH CONTROL REGISTER */
#define		T1_MCR			TIMER1->MCR.dword

#define		T1_MR0I			TIMER1->MCR.bit00
#define		T1_MR0R			TIMER1->MCR.bit01
#define		T1_MR0S			TIMER1->MCR.bit02

#define		T1_MR1I			TIMER1->MCR.bit03
#define		T1_MR1R			TIMER1->MCR.bit04
#define		T1_MR1S			TIMER1->MCR.bit05

#define		T1_MR2I			TIMER1->MCR.bit06
#define		T1_MR2R			TIMER1->MCR.bit07
#define		T1_MR2S			TIMER1->MCR.bit08

#define		T1_MR3I			TIMER1->MCR.bit09
#define		T1_MR3R			TIMER1->MCR.bit10
#define		T1_MR3S			TIMER1->MCR.bit11

//***************** TIMER 0 ********************
/** IR - INTERRUPT REGISTER */
#define		T2_IR			TIMER2->IR.dword

#define		T2_IR_MR0		TIMER2->IR.bit00
#define		T2_IR_MR1		TIMER2->IR.bit01
#define		T2_IR_MR2		TIMER2->IR.bit02
#define		T2_IR_MR3		TIMER2->IR.bit03
#define		T2_IR_CR0		TIMER2->IR.bit04
#define		T2_IR_CR1		TIMER2->IR.bit05

/** TCR - TIMER CONTROL REGISTER */
#define		T2_TCR			TIMER2->TCR.dword

#define		T2_TCR_CE		TIMER2->TCR.bit00
#define		T2_TCR_CR		TIMER2->TCR.bit01

/** CTCR - COUNT CONTROL REGISTER */
#define		T2_CTCR			TIMER2->CTCR.dword

#define		T2_TCR_TCM		TIMER2->CTCR.bits0y1
#define		T2_TCR_CIS		TIMER2->CTCR.bit02y3

/** TC - TIMER COUNTER REGISTER */
#define		T2_TC			TIMER2->TC.dword

/** PR - PRESCALE REGISTER */
#define		T2_PR			TIMER2->PR.dword

/** PC - PRESCALE COUNTER REGISTER */
#define		T2_PC			TIMER2->PC.dword

/** MR - MATCH CONTROL REGISTER */
#define		T2_MR0			TIMER2->MR[0].dword
#define		T2_MR1			TIMER2->MR[1].dword
#define		T2_MR2			TIMER2->MR[2].dword
#define		T2_MR3			TIMER2->MR[3].dword

/** MCR - MATCH CONTROL REGISTER */
#define		T2_MCR			TIMER2->MCR.dword

#define		T2_MR0I			TIMER2->MCR.bit00
#define		T2_MR0R			TIMER2->MCR.bit01
#define		T2_MR0S			TIMER2->MCR.bit02

#define		T2_MR1I			TIMER2->MCR.bit03
#define		T2_MR1R			TIMER2->MCR.bit04
#define		T2_MR1S			TIMER2->MCR.bit05

#define		T2_MR2I			TIMER2->MCR.bit06
#define		T2_MR2R			TIMER2->MCR.bit07
#define		T2_MR2S			TIMER2->MCR.bit08

#define		T2_MR3I			TIMER2->MCR.bit09
#define		T2_MR3R			TIMER2->MCR.bit10
#define		T2_MR3S			TIMER2->MCR.bit11

//***************** TIMER 3 ********************
/** IR - INTERRUPT REGISTER */
#define		T3_IR			TIMER3->IR.dword

#define		T3_IR_MR0		TIMER3->IR.bit00
#define		T3_IR_MR1		TIMER3->IR.bit01
#define		T3_IR_MR2		TIMER3->IR.bit02
#define		T3_IR_MR3		TIMER3->IR.bit03
#define		T3_IR_CR0		TIMER3->IR.bit04
#define		T3_IR_CR1		TIMER3->IR.bit05

/** TCR - TIMER CONTROL REGISTER */
#define		T3_TCR			TIMER3->TCR.dword

#define		T3_TCR_CE		TIMER3->TCR.bit00
#define		T3_TCR_CR		TIMER3->TCR.bit01

/** CTCR - COUNT CONTROL REGISTER */
#define		T3_CTCR			TIMER3->CTCR.dword

#define		T3_TCR_TCM		TIMER3->CTCR.bits0y1
#define		T3_TCR_CIS		TIMER3->CTCR.bit02y3

/** TC - TIMER COUNTER REGISTER */
#define		T3_TC			TIMER3->TC.dword

/** PR - PRESCALE REGISTER */
#define		T3_PR			TIMER3->PR.dword

/** PC - PRESCALE COUNTER REGISTER */
#define		T3_PC			TIMER3->PC.dword

/** MR - MATCH CONTROL REGISTER */
#define		T3_MR0			TIMER3->MCR[0].dword
#define		T3_MR1			TIMER3->MCR[1].dword
#define		T3_MR2			TIMER3->MCR[2].dword
#define		T3_MR3			TIMER3->MCR[3].dword

/** MCR - MATCH CONTROL REGISTER */
#define		T3_MCR			TIMER3->MCR.dword

#define		T3_MR0I			TIMER3->MCR.bit00
#define		T3_MR0R			TIMER3->MCR.bit01
#define		T3_MR0S			TIMER3->MCR.bit02

#define		T3_MR1I			TIMER3->MCR.bit03
#define		T3_MR1R			TIMER3->MCR.bit04
#define		T3_MR1S			TIMER3->MCR.bit05

#define		T3_MR2I			TIMER3->MCR.bit06
#define		T3_MR2R			TIMER3->MCR.bit07
#define		T3_MR2S			TIMER3->MCR.bit08

#define		T3_MR3I			TIMER3->MCR.bit09
#define		T3_MR3R			TIMER3->MCR.bit10
#define		T3_MR3S			TIMER3->MCR.bit11
