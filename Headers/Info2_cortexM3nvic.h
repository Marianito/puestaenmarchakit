#ifndef _registros
	#include "registros.h"
	#define		_registros
#endif

/*------------- NVIC --------------------------*/
#define		ISER 		( ( uint32_t  * ) 0xE000E100UL )

#define		ICER 		( ( uint32_t  * ) 0xE000E180UL )

typedef struct
{
	__RW register_t		ispr[4];
} nvic_ispr_t;

#define		ISPR 		( ( nvic_ispr_t  * ) 0xE000E200UL )

typedef struct
{
	__RW register_t		icpr[4];
} nvic_icpr_t;

#define		ICPR 		( ( nvic_icpr_t  * ) 0xE000E280UL )

typedef struct
{
	__RW register_t		iabr[4];
} nvic_iabr_t;

#define		IABR 		( ( nvic_iabr_t  * ) 0xE000E300UL )

typedef struct
{
	__RW register_t		ipr[28];
} nvic_ipr_t;
#define		IPR 		( ( nvic_ipr_t  * ) 0xE000E400UL )

#define		STIR_ 		( ( register_t  * ) 0xE000EF00UL )
#define		STIR		STIR_->dword
#define		INTID		STIR_->byte0


#define		EXTINT 		( ( register_t  * ) 0x400FC140UL )

#define		EINT3		EXTINT->bit03

#define		EXTMODE 		( ( register_t  * ) 0x400FC148 )

#define		EXTMODE3		EXTMODE->bit03

static inline void NVIC_HabilitarIRQ(uint32_t IRQn)
{
	ISER[((uint32_t)(IRQn) / 32)] = (1 << ((uint32_t)(IRQn) % 32)); /* Habilita la interrupcion */
}

static inline void NVIC_DeshabilitarIRQ(uint32_t IRQn)
{
	ICER[((uint32_t)(IRQn) / 32)] = (1 << ((uint32_t)(IRQn) % 32)); /* Deshabilita la interrupcion */
}
