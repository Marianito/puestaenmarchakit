/**
 	\file KitInfo2.h
 	\brief Configuracion de Infotronic
 	\author Ing. Marcelo Trujillo
 	\date 2012.04.25
*/

#include "Info2_cortexM3gpio.h"
#include "Info2_cortexM3config.h"
#include "Info2_CortexM3Systick.h"
#include "Info2_cortexM3timer.h"
#include "Info2_cortexM3nvic.h"

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>


// Varios ---------------------------------------------------------------------------------------------------
#define	ON		1
#define	OFF		0
#define	SALIDA	1
#define	ENTRADA	0

#define	GPIO	0
#define LED_TOGGLE_DELAY	100

void Inicializar( void );
void SysTickInic( void );

// Salidas --------------------------------------------------------------------------------------------------
#define		BEEPER		FIO0PIN28		//!< Beeper

#define		relay0		FIO0PIN23		//!< Relays
#define		relay1		FIO0PIN21
#define		relay2		FIO2PIN0
#define		relay3		FIO0PIN27
#define		led0		FIO2PIN1		//!< led
#define		led1		FIO2PIN2
#define		led2		FIO2PIN3

/** -------------------------------------------------------------------------------------
 *Temporizadores
 */
typedef	unsigned char		EVENTOS ;
typedef	unsigned char		TIEMPOS ;

#define		TIMERS			10				//!< Cantidad de temporizadores

#define		E_00			0
#define		E_01			1
#define		E_02			2

#define		T_00			2
#define		T_01			2
#define		T_02			2

#define 	TIMERlEDS 			0
#define 	TIMERrEFRESCOaDC	1
#define 	TIEMPOtITILEO		4
#define 	TIEMPOrEFRESCOaDC	13


#define		DECIMAS			40
#define		SEGUNDOS		10
#define		MINUTOS			60

// Teclado --------------------------------------------------------------------------------------------------
void DriverTeclado( void );
void DriverTecladoSW ( unsigned char );
unsigned char DriverTecladoHW( void );
unsigned char Teclado( void );

#define		REBOTES		4

#define		fila0		FIO0PIN20
#define		fila1		FIO3PIN25
#define		fila2		FIO1PIN27

#define		columna0	FIO1PIN24
#define		columna1	FIO1PIN21
#define		columna2	FIO1PIN18

#define		F0			0
#define		F1			3
#define		F2			1
#define		F3			2
#define		F4			4
#define		F5			5
#define		F6			6
#define		F7			7

#define		led0		FIO2PIN1		//!< led
#define		led1		FIO2PIN2
#define		led2		FIO2PIN3

#define		LED0		0				//!< Numero de led
#define		LED1		1
#define		LED2		2

// Teclado Fijo 4x1----------------------------------------------------------------------
#define		TECLAfIJA0		FIO2PIN10
#define		TECLAfIJA1		FIO0PIN18
#define		TECLAfIJA2		FIO0PIN11
#define		TECLAfIJA3		FIO2PIN13
#define		TECLAfIJA4		FIO1PIN26

/** -------------------------------------------------------------------------------------
 *Entradas
 */
#define		in0				FIO1PIN26
#define		in1				FIO4PIN29
#define		in2				FIO2PIN11

#define		Entrada0		(   BufferEntradas        & 0x01 )
#define		Entrada1		( ( BufferEntradas >> 1 ) & 0x01 )
#define		Entrada2		( ( BufferEntradas >> 2 ) & 0x01 )

#define		ACEPTAReSTADO	4
#define		ENTRADAS		3

#define		NO_KEY		0xff

#define ESTADOcERRADO	0
#define ESTADOaBRIENDO	1
#define ESTADOaBIERTO	2
#define ESTADOcERRANDO	3
#define ESTADOmAXIMO	4

#define	IDLE				0
#define	RELESeNTRADASaDC	1
#define UART0y1				2
#define PWM 				3
#define DAC					4


#define SENSOR1oFFsENSOR2oFF	0x00
#define SENSOR1oNsENSOR2oFF 	0x01
#define SENSOR1oFFsENSOR2oN		0x02
#define SENSOR1oNsENSOR2oN		0x03
#define SENSOR3oN				0x04

#define RELAY0	0
#define RELAY1	1
#define RELAY2	2
#define RELAY3	3

#define AZUL	0
#define ROJO	1
#define VERDE	2

#define 	MINpWM	0
#define		FULLpWM	100000
#define		DELTApWMmIN 0//20
#define		DELTApWMmAX 0//27
#define 	PASOSrEPETICION	10000

//Serie
#define 	TXBUFFER_SIZE	32
#define 	RXBUFFER_SIZE	32
#define 	START_TX	(U1THR = (uint8_t)PopTx())
#define 	CANTtRAMAvALIDAR 16
//#define 	TRAMAaVALIDAR "M4R14N1T0"
#define 	TRAMAaVALIDAR "UTN.BA INFO II"
//#define 	TRAMAaVALIDAR '\0'

// Codigos de Tx Rx
#define		TX_00			0xe0		// Entrada 0
#define		TX_01			0xe1		// Entrada 1
#define		TX_02			0xe2		// Entrada 2
#define		TX_03			0xe3		// Entrada 3
#define		TX_04			0xe4		// Entrada 4
#define		TX_05			0xe5		// Entrada 5
#define		TX_06			0xe6		// Entrada 6
#define		TX_07			0xe7		// Conteo de ciclo
#define		TX_08			0xe8		// Codido para transmitir programa
#define		TX_09			0xe9		// Entrada 7
#define		TOPE			8

//i2c1
#define		STARTi2C1		I2C1CONSET |= 0x20
#define		ONi2C1			I2C1CONSET |= 0x40
#define 	BUFSIZE				0x20
#define 	MAX_TIMEOUT			0x00FFFFFF

#define 	I2CMASTER			0x01
#define 	I2CSLAVE			0x02

#define		ADR_BYTE			0xA0
#define 	RD_BIT				0x01

#define 	I2C_IDLE			0
#define 	I2C_STARTED			1
#define 	I2C_RESTARTED		2
#define 	I2C_REPEATED_START	3
#define 	DATA_ACK			4
#define 	DATA_NACK			5

#define 	I2CONSET_I2EN		0x00000040  /* I2C Control Set Register */
#define 	I2CONSET_AA			0x00000004
#define 	I2CONSET_SI			0x00000008
#define 	I2CONSET_STO		0x00000010
#define 	I2CONSET_STA		0x00000020

#define 	I2CONCLR_AAC		0x00000004  /* I2C Control clear Register */
#define 	I2CONCLR_SIC		0x00000008
#define 	I2CONCLR_STAC		0x00000020
#define 	I2CONCLR_I2ENC		0x00000040

#define 	I2DAT_I2C			0x00000000  /* I2C Data Reg */
#define 	I2ADR_I2C			0x00000000  /* I2C Slave Address Reg */
#define 	I2SCLH_SCLH			0x00000080  /* I2C SCL Duty Cycle High Reg */
#define 	I2SCLL_SCLL			0x00000080  /* I2C SCL Duty Cycle Low Reg */

// Display Matricial ----------------------------------------------------------------------------------------
void Inic_Expansion4( void );
void EX4_ShiftDato( int );
void EX4_BarridoMatriz( void );
void EX4_TablaCaracteres(void);
void EX4_DspRotarIzquierda( void );
void EX4_DspRotarDerecha( void );
int EX4_Display( char * , char  );
void EX4_DspRotarAbajo( void );
void EX4_DspRotarArriba( void );
unsigned char EX4_DriverTecladoHW( void );
void EX4_Prueba ( unsigned char ) ;

#define		EX4_Control( x )	EX4_Display( NULL , x )

#define		EX4_Data		FIO1PIN25
#define		EX4_Clock		FIO1PIN19
#define		EX4_Output		FIO1PIN22

#define 	EX4_M_fila0		FIO3PIN26
#define 	EX4_M_fila1		FIO0PIN19
#define 	EX4_M_fila2		FIO1PIN20
#define 	EX4_M_fila3		FIO1PIN23
#define 	EX4_M_fila4		FIO4PIN28
#define 	EX4_M_fila5		FIO1PIN29
#define		EX4_M_fila6		FIO2PIN7

#define		EX4_M_FILA0		0
#define		EX4_M_FILA1		1
#define		EX4_M_FILA2		2
#define		EX4_M_FILA3		3
#define		EX4_M_FILA4		4
#define		EX4_M_FILA5		5
#define		EX4_M_FILA6		6

#define		ROTAR_DERECHA	0
#define		ROTAR_IZQUIERDA	1
#define		ROTAR_ARRIBA	2
#define		ROTAR_ABAJO		3

struct matriz
{
	unsigned char Fila[8];
};

#define		EX4_BUFFER		21
#define		EX4_DIGITOS		4

// LCD ------------------------------------------------------------------------------------------------------
void Dato_LCD ( void );
void Display_lcd ( char * , char  , char  );
void Inic_LCD ( void );
unsigned char PushLCD ( unsigned char , unsigned char );
int PopLCD ( void );
void Config_LCD( void );


//!< KitInfo2PR_gpio.c
void Led (char , char );
void Relays (char , char );

//!< KitInfo2FW_Teclado.c
void DriverTeclado( void );
void DriverTecladoSW ( unsigned char );
unsigned char DriverTecladoHW( void );
unsigned char DriverTecladoFijoHW( void );
void DriverTecladoFijo(void);
void DriverTecladoSWfijo ( unsigned char );
unsigned char TecladoFijo( void );

//!< KitInfo2PR_Teclado.c
unsigned char Teclado( void );

//!< KitInfo2FW_Entradas.c
void Debounce(void);

//!< KitInfo2FW_Inicializacion.c
void Inic_Teclado_BB( void );
void Inic_Relays( void );
void Inic_Entradas( void );
void Inic_Leds ( void );
void Inic_Expansion2( void );
void Inicializar( void );
void SysTickInic( void );
void InitDAC( void );
void InitADC( void );
int LeeADC( void );
int ConvertirMiliVolts( int );
void InitUART1( void );
void InitUART0( void );
void Inicializar_Timers(void);
void InitPLL ( void );
uint32_t I2CStart( void );
uint32_t I2CStop( void );
uint32_t I2CEngine( void );

//!< KitInfo2PR_I2C.c
uint8_t EX_EEPROM_READ( uint16_t );
uint8_t  EX_EEPROM_WRITE( uint16_t, uint8_t );


//!< KitInfo2PR_Timer.c
void TimerStart(int , TIEMPOS );
void TimerStop(int );
void TimerEvent(void);
void AnalizarTimer( int );

//Máquina de estados
void Maquina (void);
unsigned char evualuarSensores ( void );
void giro1Motor(void);
void giro2Motor(void);
void pararMotor(void);
void titilar( unsigned char );
void apagarLeds( void );
void ProbarRelesEntradasADC( void );
void ProbarUart0y1(void);
void apagar( void );
void pwm( void );
void dac( void );

//Serie
void AnalizarTramaUart1(void);
void TX_Key(unsigned char );
void TX_Prg(void);
void EnviarString (const char * );
int PopRx( void );
void PushRx( uint8_t );
int PopTx( void );
void PushTx( uint8_t );
uint8_t LeerBufferRX( void );
int8_t toupper(int8_t);

//I2C1
void InitI2C1( void );

//ADC
void conversor ( void );

//#define	_winstar1602a
#define _winstar1602b
#define 	RENGLON_1		0
#define 	RENGLON_2		1
#define 	TOPE_BUFFER_LCD		160


#ifdef _winstar1602a					//Agregado para el LCD Winstar WH1602A
	#define		LCD_d4			FIO2PIN4
	#define		LCD_d5			FIO2PIN5
	#define		LCD_d6			FIO0PIN5
	#define		LCD_d7			FIO0PIN10
	#define		LCD_RS			FIO0PIN4
	#define		LCD_BF			FIO1PIN28
	#define		LCD_E			FIO2PIN6
#else

#ifdef _winstar1602b
	#define		LCD_d4			FIO0PIN5
	#define		LCD_d5			FIO0PIN10
	#define		LCD_d6			FIO2PIN4
	#define		LCD_d7			FIO2PIN5
	#define		LCD_RS			FIO2PIN6
	#define		LCD_BF			FIO1PIN28
	#define		LCD_E			FIO0PIN4
#endif
#endif




#define		LCD_CONTROL		1
#define		LCD_DATA		0

#define		DIRECCION(PORT,PIN,INOUT)(PORT.FIODIR)=((INOUT)==SALIDA?((PORT.FIODIR)|(0x01<<(PIN))):((PORT.FIODIR)&~(0x01<<(PIN))))
#define		SETPIN(PORT,PIN,STAT)(PORT.FIOPIN)=(STAT==1?(PORT.FIOPIN|(0x01<<PIN)):(PORT.FIOPIN&~(0x01<<PIN)))


#define SETDAC(VAL)	DACR = (((VAL&0x03FF)<<6)|(1<<16))
#define	SENO_MUESTRAS	64
#define MAX_VALOR_DAC	1024
#define MIN_VALOR_DAC	0

#define	ADC_VAL(reg)	((reg>>04)&0x00000FFF)
#define	ADC_DONE(reg)	((reg>>31)&0x00000001)
#define	ADC_CHAN(reg)	((reg>>24)&0x00000007)
#define	ADC_CHAN0	0
#define	ADC_CHAN1	1
#define	ADC_CHAN2	2
#define	ADC_CHAN3	3
#define	ADC_CHAN4	4
#define	ADC_CHAN5	5
#define	ADC_CHAN6	6
#define	ADC_CHAN7	7


//!< ADC
#define CANTcANALES 3
#define MUESTRAS	32
#define	TERMISTOR	0
#define	ENTeXTERNA	1
#define	POTE		2
#define LIMiNFERIORmUESTRAS MUESTRAS/4
#define LIMsUPERIORmUESTRAS 3*MUESTRAS/4
#define CANTpROMmUESTRAS (LIMsUPERIORmUESTRAS - LIMiNFERIORmUESTRAS)


//para timer0
typedef enum IRQn
{
/******  Cortex-M3 Processor Exceptions Numbers ***************************************************/
  NonMaskableInt_IRQn           = -14,      /*!< 2 Non Maskable Interrupt                         */
  MemoryManagement_IRQn         = -12,      /*!< 4 Cortex-M3 Memory Management Interrupt          */
  BusFault_IRQn                 = -11,      /*!< 5 Cortex-M3 Bus Fault Interrupt                  */
  UsageFault_IRQn               = -10,      /*!< 6 Cortex-M3 Usage Fault Interrupt                */
  SVCall_IRQn                   = -5,       /*!< 11 Cortex-M3 SV Call Interrupt                   */
  DebugMonitor_IRQn             = -4,       /*!< 12 Cortex-M3 Debug Monitor Interrupt             */
  PendSV_IRQn                   = -2,       /*!< 14 Cortex-M3 Pend SV Interrupt                   */
  SysTick_IRQn                  = -1,       /*!< 15 Cortex-M3 System Tick Interrupt               */

/******  LPC17xx Specific Interrupt Numbers *******************************************************/
  WDT_IRQn                      = 0,        /*!< Watchdog Timer Interrupt                         */
  TIMER0_IRQn                   = 1,        /*!< Timer0 Interrupt                                 */
  TIMER1_IRQn                   = 2,        /*!< Timer1 Interrupt                                 */
  TIMER2_IRQn                   = 3,        /*!< Timer2 Interrupt                                 */
  TIMER3_IRQn                   = 4,        /*!< Timer3 Interrupt                                 */
  UART0_IRQn                    = 5,        /*!< UART0 Interrupt                                  */
  UART1_IRQn                    = 6,        /*!< UART1 Interrupt                                  */
  UART2_IRQn                    = 7,        /*!< UART2 Interrupt                                  */
  UART3_IRQn                    = 8,        /*!< UART3 Interrupt                                  */
  PWM1_IRQn                     = 9,        /*!< PWM1 Interrupt                                   */
  I2C0_IRQn                     = 10,       /*!< I2C0 Interrupt                                   */
  I2C1_IRQn                     = 11,       /*!< I2C1 Interrupt                                   */
  I2C2_IRQn                     = 12,       /*!< I2C2 Interrupt                                   */
  SPI_IRQn                      = 13,       /*!< SPI Interrupt                                    */
  SSP0_IRQn                     = 14,       /*!< SSP0 Interrupt                                   */
  SSP1_IRQn                     = 15,       /*!< SSP1 Interrupt                                   */
  PLL0_IRQn                     = 16,       /*!< PLL0 Lock (Main PLL) Interrupt                   */
  RTC_IRQn                      = 17,       /*!< Real Time Clock Interrupt                        */
  EINT0_IRQn                    = 18,       /*!< External Interrupt 0 Interrupt                   */
  EINT1_IRQn                    = 19,       /*!< External Interrupt 1 Interrupt                   */
  EINT2_IRQn                    = 20,       /*!< External Interrupt 2 Interrupt                   */
  EINT3_IRQn                    = 21,       /*!< External Interrupt 3 Interrupt                   */
  ADC_IRQn                      = 22,       /*!< A/D Converter Interrupt                          */
  BOD_IRQn                      = 23,       /*!< Brown-Out Detect Interrupt                       */
  USB_IRQn                      = 24,       /*!< USB Interrupt                                    */
  CAN_IRQn                      = 25,       /*!< CAN Interrupt                                    */
  DMA_IRQn                      = 26,       /*!< General Purpose DMA Interrupt                    */
  I2S_IRQn                      = 27,       /*!< I2S Interrupt                                    */
  ENET_IRQn                     = 28,       /*!< Ethernet Interrupt                               */
  RIT_IRQn                      = 29,       /*!< Repetitive Interrupt Timer Interrupt             */
  MCPWM_IRQn                    = 30,       /*!< Motor Control PWM Interrupt                      */
  QEI_IRQn                      = 31,       /*!< Quadrature Encoder Interface Interrupt           */
  PLL1_IRQn                     = 32,       /*!< PLL1 Lock (USB PLL) Interrupt                    */
  USBActivity_IRQn              = 33,       /* USB Activity interrupt                             */
  CANActivity_IRQn              = 34,       /* CAN Activity interrupt                             */
} IRQn_Type;

//Valores para configuracion del PLL:
#define CLOCK_SETUP_Value 	    1
#define SCS_Value				0x00000020
#define CLKSRCSEL_Value         0x00000001
#define PLL0_SETUP_Value        1
#define PLL0CFG_Value           0x00050063
#define PLL1_SETUP_Value        1
#define PLL1CFG_Value           0x00000023
#define CCLKCFG_Value           0x00000003
#define USBCLKCFG_Value         0x00000000
#define PCLKSEL0_Value          0x00000000
#define PCLKSEL1_Value          0x00000000
#define PCONP_Value             0x042887DE
#define CLKOUTCFG_Value         0x00000000
#define FLASHCFG_Value			0x00004000
