/* Author: Ing. Marcelo Trujillo */

#include <lpc_types.h>						//!< Contiene a uint32_t, uint16_t y uint8_t

#define     __R     volatile const          /*!< defines 'read only' permissions      */
#define     __W     volatile                /*!< defines 'write only' permissions     */
#define     __RW	volatile                /*!< defines 'read / write' permissions   */

//0x400FC1A8UL : Direccion de inicio de los registros de seleccion de los clks de los dispositivos:
//0x2009C000UL : Direccion de inicio de los registros de GPIOs
#define		GPIOs		( ( gpio_t  * ) 0x2009C000UL )

#define		PCLKSEL		( ( uint32_t  * ) 0x400FC1A8UL )
//0x4002C000UL : Direccion de inicio de los registros PINSEL
//#define		PINSEL		( ( uint32_t  * ) 0x4002C000UL )
//0x4008C000UL : Registro de conversion del DAC:
#define		DIR_DACR	( ( uint32_t  * ) 0x4008C000UL )
//0x4008C004UL : Registro de control del DAC:
#define		DIR_DACCTRL	( ( uint32_t  * ) 0x4008C004UL )
//0x4008C008UL : Registro de contador del DAC:
#define		DIR_DACCNTVAL ( ( uint32_t  * ) 0x4008C008UL )
//0x400FC0C4UL : Direccion de inicio del registro de habilitación de dispositivos:
#define 	DIR_PCONP	( ( uint32_t  * ) 0x400FC0C4UL )

//0x40034000UL: Registro de control del ADC:
#define		DIR_AD0CR	( ( uint32_t  * ) 0x40034000UL )
//0x40034030UL: Registro de Status del ADC:
#define		DIR_AD0STAT	( ( uint32_t  * ) 0x40034030UL )
//0x40034004UL: Registro de estado del ADC:
#define		DIR_AD0GDR	( ( uint32_t  * ) 0x40034004UL )
//0x4003400CUL: Registro de interrupcion del ADC
#define		DIR_AD0INTEN ( ( uint32_t  * ) 0x4003400CUL )
//0x40034010UL: Registros de estado de los ADCx
#define		AD0DR		( ( uint32_t  * ) 0x40034010UL )
//0xE0001000UL : Registro de habilitacion de las interrupciones:
#define		ISER		( ( uint32_t  * ) 0xE000E100UL )
//UART0:
//0x4001000CUL : Registro de control de la UART0:
#define		DIR_U0LCR	( ( uint32_t  * ) 0x4000C00CUL )
//0x40010014UL : Registro de recepcion de la UART0:
#define		DIR_U0LSR		( ( uint32_t  * ) 0x4000C014UL )
//0x40010000UL : Parte baja del divisor de la UART0:
#define		DIR_U0DLL	( ( uint32_t  * ) 0x4000C000UL )
//0x40010004UL : Parte alta del divisor de la UART0:
#define		DIR_U0DLM	( ( uint32_t  * ) 0x4000C004UL )
//0x40010000UL : Registro de recepcion de la UART0:
#define		DIR_U0RBR		( ( uint32_t  * ) 0x4000C000UL )
//0x40010000UL : Registro de transmision de la UART0:
#define		DIR_U0THR		( ( uint32_t  * ) 0x4000C000UL )

//UART1:
//0x4001000CUL : Registro de control de la UART1:
#define		DIR_U1LCR	( ( uint32_t  * ) 0x4001000CUL )
//0x40010014UL : Registro de recepcion de la UART1:
#define		DIR_U1LSR		( ( uint32_t  * ) 0x40010014UL )
//0x40010000UL : Parte baja del divisor de la UART1:
#define		DIR_U1DLL	( ( uint32_t  * ) 0x40010000UL )
//0x40010004UL : Parte alta del divisor de la UART1:
#define		DIR_U1DLM	( ( uint32_t  * ) 0x40010004UL )
//0x40010000UL : Registro de recepcion de la UART1:
#define		DIR_U1RBR	( ( uint32_t  * ) 0x40010000UL )
//0x40010000UL : Registro de transmision de la UART1:
#define		DIR_U1THR	( ( uint32_t  * ) 0x40010000UL )
//0x40010004UL : Registro habilitacion de interrupciones de la UART1:
#define		DIR_U1IER	( ( uint32_t  * ) 0x40010004UL )
//0x40010008UL : Registro de identificación de la interrupción de la UART1:
#define		DIR_U1IIR	( ( uint32_t  * ) 0x40010008UL )
//0x40010008UL : Registro de control de la FIFO de la UART1:
#define		DIR_U1FCR	( ( uint32_t  * ) 0x40010008UL )

//Registros del CLOCK y de sistema:
//0x400FC1A0UL: Registro de control de sistema y registro de status:
#define	DIR_SCS	( (uint32_t *) 0x400FC1A0UL)
//0x400FC104UL: Registro de configuracion del clock:
#define	DIR_CCLKCFG	( (uint32_t *) 0x400FC104UL)
//0x400FC10CUL: Registro de seleccion del clock:
#define	DIR_CLKSRCSEL	( (uint32_t *) 0x400FC10CUL)
//0x400FC1C8UL: Clock Output Config register:
#define	DIR_CLKOUTCFG	( (uint32_t *) 0x400FC1C8UL)
//0x400FC000UL: Flash access configuration:
#define	DIR_FLASHCFG	( (uint32_t *) 0x400FC000UL)

//Registros de los PLL:
//0x400FC080UL: Registro de control del PLL0:
#define	DIR_PLL0CON	( (uint32_t *) 0x400FC080UL)
//0x400FC084UL: Registro de configuracion del PLL0:
#define	DIR_PLL0CFG	( (uint32_t *) 0x400FC084UL)
//0x400FC088UL: Registro de estado del PLL0:
#define	DIR_PLL0STAT	( (uint32_t *) 0x400FC088UL)
//0x400FC08CUL: Registro de control del PLL0:
#define	DIR_PLL0FEED	( (uint32_t *) 0x400FC08CUL)
//0x400FC0A0UL: Registro de control del PLL1:
#define	DIR_PLL1CON	( (uint32_t *) 0x400FC0A0UL)
//0x400FC0A4UL: Registro de configuracion del PLL1:
#define	DIR_PLL1CFG	( (uint32_t *) 0x400FC0A4UL)
//0x400FC0A8UL: Registro de estado del PLL1:
#define	DIR_PLL1STAT	( (uint32_t *) 0x400FC0A8UL)
//0x400FC0ACUL: Registro de control del PLL1:
#define	DIR_PLL1FEED	( (uint32_t *) 0x400FC0ACUL)


//I2C1SCLL: 0x4005 C014
//I2C1SCLH: 0x4005 C010
#define		DIR_I2C1SCLL		( ( uint32_t  * ) 0x4005C014UL )
#define		DIR_I2C1SCLH		( ( uint32_t  * ) 0x4005C010UL )
#define 	DIR_I2C1STAT		( ( uint32_t  * ) 0x4005C004UL )
#define		DIR_I2C1CONSET		( ( uint32_t  * ) 0x4005C000UL )
#define		DIR_I2C1CONCLR		( ( uint32_t  * ) 0x4005C018UL )
#define		DIR_I2C1DAT			( ( uint32_t  * ) 0x4005C008UL )





typedef union
{
	__RW uint32_t dword;
	__R  uint32_t dWord;
	struct
	{
		__RW uint16_t wordL;
		__RW uint16_t wordH;
	};

	struct
	{
		__RW uint8_t  byte0;
		__RW uint8_t  byte1;
		__RW uint8_t  byte2;
		__RW uint8_t  byte3;
	};
	
	struct
	{
		__R uint8_t  byte0;
		__R uint8_t  byte1;
		__R uint8_t  byte2;
		__R uint8_t  byte3;
	}r;
	
	struct
	{
		__W uint8_t  byte0;
		__W uint8_t  byte1;
		__W uint8_t  byte2;
		__W uint8_t  byte3;
	}w;
	
	struct
	{
		__RW uint32_t	bit00:1;
		__RW uint32_t	bit01:1;
		__RW uint32_t	bit02:1;
		__RW uint32_t	bit03:1;
		__RW uint32_t	bit04:1;
		__RW uint32_t	bit05:1;
		__RW uint32_t	bit06:1;
		__RW uint32_t	bit07:1;
		__RW uint32_t	bit08:1;
		__RW uint32_t	bit09:1;
		__RW uint32_t	bit10:1;
		__RW uint32_t	bit11:1;
		__RW uint32_t	bit12:1;
		__RW uint32_t	bit13:1;
		__RW uint32_t	bit14:1;
		__RW uint32_t	bit15:1;
		__RW uint32_t	bit16:1;
		__RW uint32_t	bit17:1;
		__RW uint32_t	bit18:1;
		__RW uint32_t	bit19:1;
		__RW uint32_t	bit20:1;
		__RW uint32_t	bit21:1;
		__RW uint32_t	bit22:1;
		__RW uint32_t	bit23:1;
		__RW uint32_t	bit24:1;
		__RW uint32_t	bit25:1;
		__RW uint32_t	bit26:1;
		__RW uint32_t	bit27:1;
		__RW uint32_t	bit28:1;
		__RW uint32_t	bit29:1;
		__RW uint32_t	bit30:1;
		__RW uint32_t	bit31:1;
	};

	struct
	{
		__R uint32_t	bit00:1;
		__R uint32_t	bit01:1;
		__R uint32_t	bit02:1;
		__R uint32_t	bit03:1;
		__R uint32_t	bit04:1;
		__R uint32_t	bit05:1;
		__R uint32_t	bit06:1;
		__R uint32_t	bit07:1;
		__R uint32_t	bit08:1;
		__R uint32_t	bit09:1;
		__R uint32_t	bit10:1;
		__R uint32_t	bit11:1;
		__R uint32_t	bit12:1;
		__R uint32_t	bit13:1;
		__R uint32_t	bit14:1;
		__R uint32_t	bit15:1;
		__R uint32_t	bit16:1;
		__R uint32_t	bit17:1;
		__R uint32_t	bit18:1;
		__R uint32_t	bit19:1;
		__R uint32_t	bit20:1;
		__R uint32_t	bit21:1;
		__R uint32_t	bit22:1;
		__R uint32_t	bit23:1;
		__R uint32_t	bit24:1;
		__R uint32_t	bit25:1;
		__R uint32_t	bit26:1;
		__R uint32_t	bit27:1;
		__R uint32_t	bit28:1;
		__R uint32_t	bit29:1;
		__R uint32_t	bit30:1;
		__R uint32_t	bit31:1;
	}R;

	struct
	{
		__RW uint32_t	bits0y1:2;
		__RW uint32_t	bits2y3:2;
		__RW uint32_t	bits4y5:2;
		__RW uint32_t	bits6y7:2;
		__RW uint32_t	bits8y9:2;
		__RW uint32_t	bits10y11:2;
		__RW uint32_t	bits12y13:2;
		__RW uint32_t	bits14y15:2;
		__RW uint32_t	bits16y17:2;
		__RW uint32_t	bits18y19:2;
		__RW uint32_t	bits20y21:2;
		__RW uint32_t	bits22y23:2;
		__RW uint32_t	bits24y25:2;
		__RW uint32_t	bits26y27:2;
		__RW uint32_t	bits28y29:2;
		__RW uint32_t	bits30y31:2;
	};

	// Estructuras para el ADC
	struct
	{
		__RW uint32_t	bits0a7:8;
		__RW uint32_t	bits8a15:8;
		__RW uint32_t	bits16:1;
		__RW uint32_t	bits17a20:4;
		__RW uint32_t	bits21:1;
		__RW uint32_t	bits22a23:2;
		__RW uint32_t	bits24a26:3;
		__RW uint32_t	bits27:1;
		__RW uint32_t	bits28a31:4;
	};

	struct
	{
		__RW uint32_t	bits0a3:4;
		__RW uint32_t	bits4a15:12;
		__RW uint32_t	bits16a23:8;
		__RW uint32_t	bits24a26:3;
		__RW uint32_t	bits27a29:3;
		__RW uint32_t	bits30:1;
		__RW uint32_t	bits31:1;
	}ADC2;

	struct
	{
		__RW uint32_t	bits0a3:4;
		__RW uint32_t	bits4a7:4;
		__RW uint32_t	bits8a11:4;
	}ADC3;

}register_t;

#define SYSTEMCORECLOCK		100000000UL	// 100 MHZ

//Registro PCONP:
#define		PCONP		DIR_PCONP[0]

//Registros PCLKSEL
#define		PCLKSEL0	PCLKSEL[0]
#define		PCLKSEL1	PCLKSEL[1]

//Registros ISER:
#define		ISER0		ISER[0]
#define		ISER1		ISER[1]

//Registros I2C1
#define		I2C1SCLL		DIR_I2C1SCLL[0]
#define		I2C1SCLH		DIR_I2C1SCLH[0]
#define 	I2C1STAT		DIR_I2C1STAT[0]
#define		I2C1CONSET		DIR_I2C1CONSET[0]
#define		I2C1CONCLR		DIR_I2C1CONCLR[0]
#define		I2C1DAT			DIR_I2C1DAT[0]

/*
//Registros PINSEL:
#define		PINSEL0		PINSEL[0]
#define		PINSEL1		PINSEL[1]
#define		PINSEL2		PINSEL[2]
#define		PINSEL3		PINSEL[3]
#define		PINSEL4		PINSEL[4]
#define		PINSEL5		PINSEL[5]
#define		PINSEL6		PINSEL[6]
#define		PINSEL7		PINSEL[7]
#define		PINSEL8		PINSEL[8]
#define		PINSEL9		PINSEL[9]
*/

//Estados de PINSEL:
#define		PINSEL_GPIO		0
#define		PINSEL_FUNC1	1
#define		PINSEL_FUNC2	2
#define		PINSEL_FUNC3	3

//Registros del DAC:
#define		DACR		DIR_DACR[0]
#define		DACCTRL		DIR_DACCTRL[0]
#define		DACCNTVAL	DIR_DACCNTVAL[0]

//Registros del ADC:
#define		AD0CR		DIR_AD0CR[0]
#define		AD0STAT		DIR_AD0STAT[0]
#define		AD0GDR		DIR_AD0GDR[0]
#define		AD0INTEN	DIR_AD0INTEN[0]

#define		AD0DR0		AD0DR[0]
#define		AD0DR1		AD0DR[1]
#define		AD0DR2		AD0DR[2]
#define		AD0DR3		AD0DR[3]
#define		AD0DR4		AD0DR[4]
#define		AD0DR5		AD0DR[5]
#define		AD0DR6		AD0DR[6]
#define		AD0DR7		AD0DR[7]

//Registros GPIOs:
#define		GPIO0		GPIOs[0]
#define		GPIO1		GPIOs[1]
#define		GPIO2		GPIOs[2]
#define		GPIO3		GPIOs[3]
#define		GPIO4		GPIOs[4]

//ADC y DAC:
#define DAC_PORT	GPIO0
#define DAC_PIN		26

#define	ADC0_PORT	GPIO0
#define ADC0_PIN	23

#define	ADC1_PORT	GPIO0
#define ADC1_PIN	24

#define	ADC2_PORT	GPIO0
#define ADC2_PIN	25

#define	ADC3_PORT	GPIO0
#define ADC3_PIN	26

#define	ADC4_PORT	GPIO1
#define ADC4_PIN	30

#define	ADC5_PORT	GPIO1
#define ADC5_PIN	31

#define	ADC6_PORT	GPIO0
#define ADC6_PIN	3

#define	ADC7_PORT	GPIO0
#define ADC7_PIN	2

//Registros de la UART0:
#define		U0THR		DIR_U0THR[0]
#define		U0RBR		DIR_U0RBR[0]
#define		U0LCR		DIR_U0LCR[0]
#define		U0LSR		DIR_U0LSR[0]
#define		U0DLL		DIR_U0DLL[0]
#define		U0DLM		DIR_U0DLM[0]

#define		U0RDR		(U0LSR&0x01)
#define		U0THRE		((U0LSR&0x20)>>5)

//Registros de la UART1:
#define		U1THR		DIR_U1THR[0]
#define		U1RBR		DIR_U1RBR[0]
#define		U1LCR		DIR_U1LCR[0]
#define		U1LSR		DIR_U1LSR[0]
#define		U1DLL		DIR_U1DLL[0]
#define		U1DLM		DIR_U1DLM[0]
#define		U1IER		DIR_U1IER[0]
#define		U1IIR		DIR_U1IIR[0]
#define		U1FCR		DIR_U1FCR[0]

#define		U1RDR		(U1LSR&0x01)
#define		U1THRE		((U1LSR&0x20)>>5)

#define		U1RS485CTRL		DIR_U1RS485CTRL[0]
#define		U1RS485ADRMATCH	DIR_U1RS485ADRMATCH[0]
#define		U1RS485DLY		DIR_U1RS485DLY[0]

//Registro de status y configuracion del sistema:
#define	SCS			DIR_SCS[0]
#define FLASHCFG	DIR_FLASHCFG[0]

//Registros de control del CLOCK:
#define	CCLKCFG		DIR_CCLKCFG[0]
#define	CLKSRCSEL	DIR_CLKSRCSEL[0]
#define	CLKOUTCFG	DIR_CLKOUTCFG[0]

//PLL0:
#define	PLL0CON		DIR_PLL0CON[0]
#define	PLL0CFG		DIR_PLL0CFG[0]
#define	PLL0STAT	DIR_PLL0STAT[0]
#define	PLL0FEED	DIR_PLL0FEED[0]

//PLL1:
#define	PLL1CON		DIR_PLL1CON[0]
#define	PLL1CFG		DIR_PLL1CFG[0]
#define	PLL1STAT	DIR_PLL1STAT[0]
#define	PLL1FEED	DIR_PLL1FEED[0]
