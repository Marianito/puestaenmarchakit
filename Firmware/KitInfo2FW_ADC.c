/**
 	\file KitInfo2FW_ADC.c
 	\brief Driver Conversor AD
 	\details Placa base
 	\author Marianito
 	\date 2013.04.26
*/

#include "KitInfo2.h"

extern uint32_t valorADC[ ];
extern uint32_t medida[ CANTcANALES ][  MUESTRAS];
extern volatile uint8_t flagADCext, flagADCpote, flagADCterm;

int LeeADC ( void )
{
	static int resultado = 0;
	static int registro[3];
	static unsigned char i;


	switch(i)
	{
	case TERMISTOR:
		registro[i] = AD0DR1;		//Termistor
		break;
	case ENTeXTERNA:
		registro[i] = AD0DR2;		//Externa
		break;
	case POTE:
		registro[i] = AD0DR5;	//Pote
		break;
	default:
		break;
	}

	if ( ADC_DONE (registro[i]) )
		{
		resultado = ( registro[i] >> 4 ) & 0x0000FFFF;
		i ++;
		i %= CANTcANALES;
		return resultado;
		}
	/*
	 //Probar individualmente cada canal

	//registro[TERMISTOR] = AD0DR1;		//Termistor
	registro[ENTeXTERNA] = AD0DR2;		//Externa
	//registro[POTE] = AD0DR5;	//Pote
	if ( ADC_DONE (registro[ENTeXTERNA]) )
			{
			resultado = ( registro[ENTeXTERNA] >> 4 ) & 0x0000FFFF;
			i ++;
			i %= CANTcANALES;

			}

	return resultado;
*/
	return 0;

}

int ConvertirMiliVolts( int valor)
{
	return (valor * 3300/4086);
}



void ADC_IRQHandler(void)
{
	//uint32_t lecturaADC, statusADC, regINT;
	uint32_t lecturaADC;
	static uint8_t indPote = 0;
	static uint8_t indTerm = 0;
	static uint8_t indExt = 0;

	//statusADC = AD0STAT;
	lecturaADC = AD0GDR;
	//regINT = AD0INTEN;

	switch( (lecturaADC >> 24) & 0x07 )
	{
	case ADC_CHAN1:

			lecturaADC = (AD0DR1 >> 4) & 0x0FFF;

			if( !flagADCterm )
			{
				medida[ 0 ][ indTerm ] = lecturaADC;

				indTerm ++;
				if( indTerm == MUESTRAS )
				{
					flagADCterm = 1;
					indTerm = 0;

				}
			}

		//indTerm %= MUESTRAS;
		/*
		AD0CR &= 0xFFFFFF80;
		AD0CR |= 0x00000004;
		*/
		AD0CR &= ~(0x1F<<0);
		AD0CR |= 0x00000004;	//Channel 2 turn

		break;
	case ADC_CHAN2:

			lecturaADC = (AD0DR2 >> 4) & 0x0FFF;

			if( !flagADCext )
			{
				medida[ 1 ][ indExt ] = lecturaADC;

				indExt ++;
				if( indExt == MUESTRAS )
				{
					flagADCext = 1;
					indExt = 0;

				}
			}

		//indExt %= MUESTRAS;
		/*
		AD0CR &= 0xFFFFFF80;
		AD0CR |= 0x00000020;	//Channel 5 turn
		*/
		AD0CR &= ~(0x1F<<0);
		AD0CR |= 0x00000020;	//Channel 5 turn
		break;
	case ADC_CHAN5:

			lecturaADC = (AD0DR5 >> 4) & 0x0FFF;

			if( !flagADCpote )
			{
				medida[ 2 ][ indPote ] = lecturaADC;
				indPote ++;
				if( indPote == MUESTRAS )
				{
				indPote = 0;
				flagADCpote = 1;
				}
			}




				//indPote %= MUESTRAS;
		/*
		AD0CR &= 0xFFFFFF80;
		AD0CR |= 0x00000002;	//Channel 1 turn
		*/
		AD0CR &= ~(0x1F<<0);
		AD0CR |= 0x00000002;	//Channel 1 turn
		break;
	default:
		break;

	}

}

void conversor ( void )
{
	uint8_t i, j;
	uint32_t aux;
	unsigned long promedio = 0;


	if( flagADCterm )
	{

		for( i = 1; i < MUESTRAS; i ++)
			for( j = 0; j < MUESTRAS - i; j ++ )
				if( medida[ 0 ][ j ] > medida[ 0 ][ j + 1 ])
				{
					aux = medida[ 0 ][ j ];
					medida[ 0 ][ j ] = medida[ 0 ][ j + 1 ];
					medida[ 0 ][ j + 1 ] = aux;

				}

		for( i = LIMiNFERIORmUESTRAS; i < LIMsUPERIORmUESTRAS;  i++)
			promedio += medida[ 0 ][ i ];

		promedio /= CANTpROMmUESTRAS;
		valorADC[0] = promedio;
		flagADCterm = 0;


	}

	if( flagADCext )
	{

		for( i = 1; i < MUESTRAS; i ++)
			for( j = 0; j < MUESTRAS - i; j ++ )
				if( medida[ 1 ][ j ] > medida[ 1 ][ j + 1 ])
				{
					aux = medida[ 1 ][ j ];
					medida[ 1 ][ j ] = medida[ 1 ][ j + 1 ];
					medida[ 1 ][ j + 1 ] = aux;

				}
		for( i = LIMiNFERIORmUESTRAS; i < LIMsUPERIORmUESTRAS;  i++)
			promedio += medida[ 1 ][ i ];

		promedio /= CANTpROMmUESTRAS;
		valorADC[1] = promedio;
		flagADCext = 0;

	}

	if( flagADCpote )
	{

		for( i = 1; i < MUESTRAS; i ++)
			for( j = 0; j < MUESTRAS - i; j ++ )
				if( medida[ 2 ][ j ] > medida[ 2 ][ j + 1 ] )
				{
					aux = medida[ 2 ][ j ];
					medida[ 2 ][ j ] = medida[ 2 ][ j + 1 ];
					medida[ 2 ][ j + 1 ] = aux;

				}
		for( i = LIMiNFERIORmUESTRAS; i < LIMsUPERIORmUESTRAS;  i++)
			promedio += medida[ 2 ][ i ];

		promedio /= CANTpROMmUESTRAS;
		valorADC[2] = promedio;
		flagADCpote = 0;

	}


}
