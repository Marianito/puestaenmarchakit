/**
 	\file KitInfo2FW_LCD.c
 	\brief Drivers del LCD
 	\details Validas para LCD de 4 bits de direccionamiento
 	\author Ing. Marcelo Trujillo
 	\date 2012.04.25
*/
#include "KitInfo2.h"

extern volatile uint32_t I2CMasterState;
extern volatile uint32_t I2CSlaveState;

extern volatile uint32_t I2CCmd;
extern volatile uint32_t I2CMode;

extern volatile uint8_t I2CMasterBuffer[];
extern volatile uint8_t I2CSlaveBuffer[];
extern volatile uint32_t I2CCount;
extern volatile uint32_t I2CReadLength;
extern volatile uint32_t I2CWriteLength;

extern volatile uint32_t RdIndex;
extern volatile uint32_t WrIndex;

void InitI2C1( void )
{
	/*
	//P0.19 y P0.20 son para la Eemprom interna en el stick
	FIO0DIR19 = ENTRADA;		//sda1 P0.19
	FIO0DIR20 = SALIDA;		//scl1 P0.20
	P019_PINSEL = PINSEL_FUNC3;
	P020_PINSEL = PINSEL_FUNC3;
	P019_PINMODE = 2;	//no pull-up, no pull-down resistors
	P020_PINMODE = 2;	//no pull-up, no pull-down resistors
	P019_OD = 1;			//open drain
	P020_OD = 1;			//open drain
	*/

	FIO0DIR0 = ENTRADA;		//sda1 P0.0
	FIO0DIR1 = SALIDA;		//scl1 P0.1
	P00_PINSEL = PINSEL_FUNC3;
	P01_PINSEL = PINSEL_FUNC3;
	P00_PINMODE = 2;	//no pull-up, no pull-down resistors
	P01_PINMODE = 2;	//no pull-up, no pull-down resistors
	P00_OD = 1;			//open drain
	P01_OD = 1;			//open drain


	PCLKSEL1 &= ~(0x03<<6);	//PCLK_peripheral = CCLK/4
	I2C1SCLH = 0x7D;		//scl1 100KHz, PCLKi2c1 = clk/4 = 25MHz
	I2C1SCLL = 0x7D;		// I2C1SCLH + I2C1SCLL = 250 -> 25MHz/250 = 100KHz
	ONi2C1;
	//STARTi2C1;
	NVIC_HabilitarIRQ(I2C1_IRQn);

}



void I2C1_IRQHandler(void) {


	switch( I2C1STAT )
	{
		case 0x08:			/* A Start condition is issued. */
		I2C1DAT = I2CMasterBuffer[0];
		I2C1CONCLR = 0x28;
		//LPC_I2C1->I2CONCLR = (I2CONCLR_SIC | I2CONCLR_STAC);
		I2CMasterState = I2C_STARTED;
		break;

		case 0x10:			/* A repeated started is issued */
		if ( ! I2CCmd )
		{
			I2C1DAT = I2CMasterBuffer[3];
		}
		I2C1CONCLR= 0x28;
		I2CMasterState = I2C_RESTARTED;
		break;

		case 0x18:			/* Regardless, it's a ACK */
		if ( I2CMasterState == I2C_STARTED )
		{
			I2C1DAT = I2CMasterBuffer[1+WrIndex];
			WrIndex++;
			I2CMasterState = DATA_ACK;
		}
		I2C1CONCLR = 0x08;
		break;

		case 0x28:	/* Data byte has been transmitted, regardless ACK or NACK */
		case 0x30:
		if ( WrIndex != I2CWriteLength )
		{
			I2C1DAT = I2CMasterBuffer[1+WrIndex]; /* this should be the last one */
		  WrIndex++;
		  if ( WrIndex != I2CWriteLength )
		  {
			I2CMasterState = DATA_ACK;
		  }
		  else
		  {
			I2CMasterState = DATA_NACK;
			//LPC_I2C1->I2CONSET = I2CONSET_STO;	//Prueba para ver si mando el stop
			if ( I2CReadLength != 0 )
			{
				I2C1CONSET = I2CONSET_STA;	/* Set Repeated-start flag */
			  I2CMasterState = I2C_REPEATED_START;
			}
		  }
		}
		else
		{
		  if ( I2CReadLength != 0 )
		  {
			I2C1CONSET = I2CONSET_STA;	/* Set Repeated-start flag */
			I2CMasterState = I2C_REPEATED_START;
		  }
		  else
		  {
			I2CMasterState = DATA_NACK;
			I2C1CONSET = I2CONSET_STO;      /* Set Stop flag */
		  }
		}
		I2C1CONCLR = I2CONCLR_SIC;
		break;

		case 0x40:	/* Master Receive, SLA_R has been sent */
		I2C1CONSET = I2CONSET_AA;	/* assert ACK after data is received */
		I2C1CONCLR = I2CONCLR_SIC;
		break;

		case 0x50:	/* Data byte has been received, regardless following ACK or NACK */
		case 0x58:
		I2CMasterBuffer[4+RdIndex] = I2C1DAT;
		RdIndex++;
		if ( RdIndex != I2CReadLength )
		{
		  I2CMasterState = DATA_ACK;
		}
		else
		{
		  RdIndex = 0;
		  I2CMasterState = DATA_NACK;

		}
		I2C1CONSET = I2CONSET_AA;	/* assert ACK after data is received */
		I2C1CONCLR = I2CONCLR_SIC;
		break;

		case 0x20:		/* regardless, it's a NACK */

		case 0x48:
		I2C1CONCLR = I2CONCLR_SIC;
		I2CMasterState = DATA_NACK;
		break;

		case 0x38:		/* Arbitration lost, in this example, we don't
						deal with multiple master situation */
		default:
		I2C1CONCLR = I2CONCLR_SIC;
		break;
  }

	/*
	switch( I2C1STAT )
	{

	//State: 0x00  Bus Error. Enter not addressed Slave mode and release bus.

		case 0x00 :			//no se envio ninguna direccion y el buss esta libre
			I2C1CONSET=0x14;
			I2C1CONCLR=0x08;
			break;

	//State: 0x08
	//A START condition has been transmitted. The Slave Address + R/W bit will now be	transmitted.

		case 0x08: 			// se envio la condicion de START. COMIENZA LA COMUNICACION
			//cargar_direccion();
			I2C1DAT=0xA0;		//1010 A2 A1 A0 R/W
			I2C1CONSET=0x04;
			I2C1CONCLR=0X08;
			break;

		case 0x10: 			//idem anterior, solo que indica que se envio de nuevo una condicion de START
			//cargar_direccion();
			I2C1DAT=0xA0;
			I2C1CONSET=0x04;
			I2C1CONCLR=0X08;
			break;

	//State: 0x18
	//Previous state was State 0x08 or State 0x10, Slave Address + Write has been transmitted,
	//ACK has been received. The first data byte will be transmitted.
	//1. Load I2DAT with first data byte from Master Transmit buffer.
	//2. Write 0x04 to I2CONSET to set the AA bit.
	//3. Write 0x08 to I2CONCLR to clear the SI flag.
	//4. Increment Master Transmit buffer pointer.
	//5. Exit

		case 0x18:


			I2C1DAT=0x7f;
			I2C1CONSET=0x04;		//Pruebo sacarlo
			I2C1CONCLR=0x08;
			break;



	//State: 0x28
	//Data has been transmitted, ACK has been received. If the transmitted data was the last
	//data byte then transmit a STOP condition, otherwise transmit the next data byte.
	//1. Decrement the Master data counter, skip to step 5 if not the last data byte.
	//2. Write 0x14 to I2CONSET to set the STO and AA bits.
	//3. Write 0x08 to I2CONCLR to clear the SI flag.
	//4. Exit
	//5. Load I2DAT with next data byte from Master Transmit buffer.
	//6. Write 0x04 to I2CONSET to set the AA bit.
	//7. Write 0x08 to I2CONCLR to clear the SI flag.
	//8. Increment Master Transmit buffer pointer
	//9. Exit


		case 0x28:


			I2C1CONSET=0x14;
			I2C1CONCLR=0x08;

			//estado_temp=ESCRITO;

			break;

	//State: 0x40 Previous state was State 08 or State 10. Slave Address + Read has been transmitted,
	//ACK has been received. Data will be received and ACK returned.

		case 0x40 :

			I2C1CONSET=0x04;
			I2C1CONCLR=0x08;
			break;

	//State:0x48 Slave Address+Read has been transmitted NOT ACK has been received A STOP condition will be transmitted

		case 0x48:
			I2C1CONSET=0x14;
			I2C1CONCLR=0x08;
			break;

	// State: 0x50
	//Data has been received, ACK has been returned. Data will be read from I2DAT. Additional
	//data will be received. If this is the last data byte then NOT ACK will be returned, otherwise ACK will be returned.
		 //1. Read data byte from I2DAT into Master Receive buffer.
		 //2. Decrement the Master data counter, skip to step 5 if not the last data byte.
		 //3. Write 0x0C to I2CONCLR to clear the SI flag and the AA bit.
		 //4. Exit
		 //5. Write 0x04 to I2CONSET to set the AA bit.
		 //6. Write 0x08 to I2CONCLR to clear the SI flag.
		 //7. Increment Master Receive buffer pointer
		 //8. Exit

		case 0x50:


			I2C1CONCLR=0x0C;
			I2C1CONSET=0x20;				//envio una nueva condicion de start una vez que recibi el dato de esa direccion
			break;





	}
	*/
}

/*****************************************************************************
** Function name:		I2CStart
**
** Descriptions:		Create I2C start condition, a timeout
**				value is set if the I2C never gets started,
**				and timed out. It's a fatal error.
**
** parameters:			None
** Returned value:		true or false, return false if timed out
**
*****************************************************************************/
uint32_t I2CStart( void )
{
  uint32_t timeout = 0;
  uint32_t retVal = 0;

  /*--- Issue a start condition ---*/
  I2C1CONSET = I2CONSET_STA;	/* Set Start flag */

  /*--- Wait until START transmitted ---*/
  while( 1 )
  {
	if ( I2CMasterState == I2C_STARTED )
	{
	  retVal = 1;
	  break;
	}
	if ( timeout >= MAX_TIMEOUT )
	{
	  retVal = 0;
	  break;
	}
	timeout++;
  }
  return( retVal );
}

/*****************************************************************************
** Function name:		I2CStop
**
** Descriptions:		Set the I2C stop condition, if the routine
**				never exit, it's a fatal bus error.
**
** parameters:			None
** Returned value:		true or never return
**
*****************************************************************************/
uint32_t I2CStop( void )
{
  I2C1CONSET = I2CONSET_STO;  /* Set Stop flag */
  I2C1CONCLR = I2CONCLR_SIC;  /* Clear SI flag */

  /*--- Wait for STOP detected ---*/
  while( I2C1CONSET & I2CONSET_STO );
  return 1;
}



/*****************************************************************************
** Function name:		I2CEngine
**
** Descriptions:		The routine to complete a I2C transaction
**				from start to stop. All the intermitten
**				steps are handled in the interrupt handler.
**				Before this routine is called, the read
**				length, write length, I2C master buffer,
**				and I2C command fields need to be filled.
**				see i2cmst.c for more details.
**
** parameters:			None
** Returned value:		true or false, return false only if the
**				start condition can never be generated and
**				timed out.
**
*****************************************************************************/
uint32_t I2CEngine( void )
{
  I2CMasterState = I2C_IDLE;
  RdIndex = 0;
  WrIndex = 0;
  if ( I2CStart() != 1 )
  {
	I2CStop();
	return ( 0 );
  }

  while ( 1 )
  {
	if ( I2CMasterState == DATA_NACK )
	{
	  I2CStop();
	  break;
	}
  }
  return ( 1 );
}

