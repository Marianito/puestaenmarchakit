/**
 	\file KitInfo2FW_Inicializacion.c
 	\brief Driver Configuracion del HW e inicializacion de la aplicacion
 	\details Placa base + Expansion 4
 	\author Ing. Marcelo Trujillo
 	\date 2012.04.25
*/

#include "KitInfo2.h"

extern volatile uint32_t Ticks;
extern uint32_t buffer_seno[];
extern uint8_t flagSeno;
extern char ValidarTrama[];
extern uint8_t CantidadCaracteresTrama;
extern volatile uint8_t flagADCext, flagADCpote, flagADCterm;

void Inicializar( void )
{
	uint32_t i;
	Ticks = 100;

	P023_PINSEL = GPIO;	FIO0DIR23 = SALIDA;
	P021_PINSEL = GPIO;	FIO0DIR21 = SALIDA;
	P20_PINSEL = GPIO;	FIO2DIR0 = SALIDA;
	P027_PINSEL = GPIO;	FIO0DIR27 = SALIDA;
	P028_PINSEL = GPIO;	FIO0DIR28 = SALIDA;

	P21_PINSEL = GPIO;	FIO2DIR1 = SALIDA;
	P22_PINSEL = GPIO;	FIO2DIR2 = SALIDA;
	P23_PINSEL = GPIO;	FIO2DIR3 = SALIDA;


	//Teclado fijo
	P210_PINSEL = GPIO;	FIO2DIR10 = ENTRADA; 	//Teclado4x1Fijo0
	P018_PINSEL = GPIO;	FIO0DIR18 = ENTRADA; 	//Teclado4x1Fijo1
	P011_PINSEL = GPIO;	FIO0DIR11 = ENTRADA; 	//Teclado4x1Fijo2
	P213_PINSEL = GPIO;	FIO2DIR13 = ENTRADA; 	//Teclado4x1Fijo3
	P126_PINSEL = GPIO;	FIO1DIR26 = ENTRADA; 	//PulsadorRC

	InitPLL( );
	SysTickInic( );
	Inic_LCD( ) ;
	Inic_Expansion4( );
	EX4_TablaCaracteres();
	InitDAC( );
	InitADC( );
	InitUART1( );
	InitUART0( );
	InitI2C1( );
	Inicializar_Timers( );


	BEEPER = ON;
	BEEPER = OFF;
	relay0 = OFF;
	relay1 = OFF;
	relay2 = OFF;
	relay3 = OFF;
	led0 = OFF;
	led1 = OFF;
	led2 = OFF;
	BEEPER = ON;

	for ( i = 0 ; i < SENO_MUESTRAS ; i++ )
	    	buffer_seno[i] = ( MAX_VALOR_DAC/2 ) * ( 1 + sin( (2 * 3.1416 * i)  / SENO_MUESTRAS ) );

	flagSeno = 0;


}

void Inic_Expansion4( void )
{
	// Display de Matriz de puntos Señales de control
	P125_PINSEL = GPIO;	FIO1DIR25 = SALIDA;			// Expansion 7
	P122_PINSEL = GPIO;	FIO1DIR22 = SALIDA;			// Expansion 8
	P119_PINSEL = GPIO;	FIO1DIR19 = SALIDA;			// Expansion 9

	P326_PINSEL = GPIO;	FIO3DIR26 = SALIDA;			// Expansion 6
	P325_PINMODE = 0;								// PULL-UP
	P019_PINSEL = GPIO;	FIO0DIR19 = SALIDA;			// Expansion 5
	P120_PINSEL = GPIO;	FIO1DIR20 = SALIDA;			// Expansion 4
	P123_PINSEL = GPIO;	FIO1DIR23 = SALIDA;			// Expansion 3
	P428_PINSEL = GPIO;	FIO4DIR28 = SALIDA;			// Expansion 2
	P129_PINSEL = GPIO;	FIO1DIR29 = SALIDA;			// Expansion 1
	P27_PINSEL = GPIO;	FIO2DIR7 = SALIDA;			// Expansion 0

	P020_PINSEL = GPIO;	FIO0DIR20 = ENTRADA;		// Expansion 10
	P325_PINSEL = GPIO;	FIO3DIR25 = ENTRADA;		// Expansion 11
	P325_PINMODE = 0;								// PULL-UP
	P127_PINSEL = GPIO;	FIO1DIR27 = ENTRADA;		// Expansion 12
	P124_PINSEL = GPIO;	FIO1DIR24 = SALIDA;			// Expansion 13
	P121_PINSEL = GPIO;	FIO1DIR21 = SALIDA;			// Expansion 14
	P118_PINSEL = GPIO;	FIO1DIR18 = SALIDA;			// Expansion 15
}

void InitDAC ( void )
{
	//1.- Selecciono el clock del DAC como 25MHz:
	PCLKSEL0 &= ~(0x03<<22);
	//2.- Configuro los pines del DAC
	//DAC : P0[26]->PINSEL1: 20:21
	PINSEL1 |= PINSEL_FUNC2 << 20;
	//Pongo una R de pull down en el pin:
	PINMODE1 |= PINSEL_FUNC3 << 20;
	//3.- Configuro el DAC:
	DACCNTVAL = 0x000000FA; //CNTVAL = 250 => 25MHz/250 = 10kHz
	DACCTRL = 0x00000006; 	//DBLBUF_ENA = 1, CNT_ENA = 1
}

void InitADC ( void )
{

	//1.- Activo la alimentacion del dispositivo desde el registro PCONP:
	PCONP |= 1<<12;
	//2.- Selecciono el clock del ADC como 25MHz:
	PCLKSEL0 &= ~(0x03<<24);
	//3.- Y el divisor como 1, para muestrear a 200kHz:
	AD0CR |= (1<<8);
	//4.- Configuro los pines del ADC0
	//ADC0.5 (pote) : P1[31]->PINSEL3: 30:31
	PINSEL3 |= PINSEL_FUNC3 << 30;
	//ADC0.1 (Termistor) : P0[24]->PINSEL1: 16:17
	//PINSEL1 &= ~(0x03 << 16);
	PINSEL1 |= PINSEL_FUNC1 << 16;
	//ADC0.2 (Externa) : P0[25]->PINSEL1: 18:19
	//PINSEL1 &= ~(0x03 << 18);
	PINSEL1 |= PINSEL_FUNC1 << 18;
	//P131_PINSEL = GPIO;	FIO1DIR31 = ENTRADA;
	//DIRECCION(ADC5_PORT,ADC5_PIN,ENTRADA);
	//DIRECCION(ADC1_PORT,ADC1_PIN,ENTRADA);
	//5.- NO ACTIVO LAS INTERRUPCIONES:
	AD0INTEN &= 0xFFFFFE00;
	//5.- INTERRUPCIONES ACTIVAS: ADINTEN1; ADINTEN2 y ADINTEN5
	//AD0INTEN &= 0x0;
	AD0INTEN |= 0x00000026;
	//6.- Selecciono que voy a tomar muestras de los canales AD0.1, AD0.2 y AD0.5:
	AD0CR |= 0x00000020;
	//7.- Activo el ADC (PDN = 1):
	AD0CR |= 1<<21;
	//8.- Selecciono que el ADC muestree solo, con BURST = 1 y START = 000:
	//AD0CR &= ~(0x0F<<24);
	//AD0CR &= ~(0x07<<24);
	//Burst
	//AD0CR |= 1<<16;


	//Start of conversion
	AD0CR |= 1<<24;
	/*
	flagADCext = 0;
	flagADCpote = 0;
	flagADCterm = 0;
	*/
	NVIC_HabilitarIRQ(ADC_IRQn); 	// Habilito interrupcion del Timer2 en el vector de interrupciones.
}

void InitUART0 ( void )
{
	//1.- Registro PCONP (0x400FC0C4) - bit 3 en 1 prende la UART:
	PCONP |= 0x01<<3;
	//2.- Registro PCLKSEL0 (0x400FC1A8) - bits 6 y 7 en 0 seleccionan que el clk de la UART0 sea 25MHz:
	PCLKSEL0 &= ~(0x03<<6);
	//3.- Registro U1LCR (0x4001000C) - transmision de 8 bits, 1 bit de stop, sin paridad, sin break cond, DLAB = 1:
	U0LCR =0x00000083;
	//4.- Registros U1DLL (0x40010000) y U1DLM (0x40010004):
	U0DLM = 0;
	U0DLL = 0xA3;
	//6.- Registros PINSEL0 (0x4002C000) y PINSEL1 (0x4002C004) - habilitan las funciones especiales de los pines:
	//TX1D : PIN ??	-> 		P0[2]	-> PINSEL0: 04:05
	PINSEL0 |= PINSEL_FUNC1 << 4;
	//RX1D : PIN ??	-> 		P0[3]	-> PINSEL1: 06:07
	PINSEL0 |= PINSEL_FUNC1 << 6;

	FIO0DIR2 = SALIDA;	//TX0D
	FIO0DIR3 = ENTRADA;	//RX0D
	//5.- Registro U1LCR, pongo DLAB en 0:
	U0LCR = 0x03;
}
void InitUART1 ( void )
{
	uint8_t i;
/*
	//TODO: Completar Inicializacion de UART1
	//1.- Registro PCONP: Energizo la UART:
	PCONP |= 0x01<<4;

	//2.- Registro PCLKSEL0 (0x400FC1A8) - selecciono el clk de la UART (recordar que cclk = 100MHz)
	PCLKSEL0 &= ~(0x03<<8);

	//3.- Registro U1LCR (0x4001000C) - transmision de 8 bits, 1 bit de stop, con un 1 en el bit de paridad, sin break cond, DLAB = 1:
	U1LCR =0x00000083;	//Sin paridad por ahora

	//4.- Registros U1DLL (0x40010000) y U1DLM (0x40010004): 9600!
	U1DLM = 0;
	U1DLL = 0xA3;

	//5.- Registros PINSEL0 (0x4002C000) y PINSEL1 (0x4002C004) - habilitan las funciones especiales de los pines:
	//TX1D : P0[15]
	PINSEL0 |= PINSEL_FUNC1 << 30;

	//RX1D : P0[16] ubicado en PINSEL1
	PINSEL1 |= PINSEL_FUNC1 << 0;
	
	//Pablo
	//SetPINSEL(P0,15,PINSEL_FUNC1);
	//RX1D : PIN ??	-> 		P0[16]	-> PINSEL1: 01:00
	//SetPINSEL(P0,16,PINSEL_FUNC1);
	
	//FIO0DIR15 = SALIDA;	//TX1D
	//FIO0DIR16 = ENTRADA;	//RX1D

	//6. Habilito las interrupciones (En la UART -IER- y en el NVIC -ISER)
	U1LCR = 0x3;
	//U1IER |= 0x06;		//Ver si habilité bien interrupción por RX y TX
	//ISER0 |= 0x40;		//Ver si habilité bien NVIC

	//Pablo
	
	//7. Habilito las interrupciones (En la UART -IER- y en el NVIC -ISER)
//	U1IER = 0x03;
//	ISER0 |= (1<<6);
	

*/
	//1.- Registro PCONP: Energizo la UART:
			PCONP |= 0x01<<4;

			//2.- Registro PCLKSEL0 (0x400FC1A8) - selecciono el clk de la UART (recordar que cclk = 100MHz)
			PCLKSEL0 &= ~(0x03<<8);

			//3.- Registro U1LCR (0x4001000C) - transmision de 8 bits, 1 bit de stop, con un 1 en el bit de paridad, sin break cond, DLAB = 1:
			U1LCR =0x00000083;	//Sin paridad por ahora

			//4.- Registros U1DLL (0x40010000) y U1DLM (0x40010004): 9600!
			U1DLM = 0;
			U1DLL = 0xA2;

			//5.- Registros PINSEL0 (0x4002C000) y PINSEL1 (0x4002C004) - habilitan las funciones especiales de los pines:
			//TX1D : P0[15]
			PINSEL0 |= PINSEL_FUNC1 << 30;

			//RX1D : P0[16] ubicado en PINSEL1
			PINSEL1 |= PINSEL_FUNC1 << 0;

			//6. Habilito las interrupciones (En la UART -IER- y en el NVIC -ISER)
			U1LCR = 0x3;
			U1IER = 0x03;		//Ver si habilité bien interrupción por RX y TX
			ISER0 |= 0x40;		//Ver si habilité bien NVIC

			ValidarTrama[ 0 ] = '\0';


			strcpy(ValidarTrama, TRAMAaVALIDAR);

			for(i = 0; ValidarTrama[ i ]; i++ );
			CantidadCaracteresTrama = i;






}

void Inicializar_Timers(void)
{
	/**/
	PCONP |= 1 << 1; 				// Habilitar Timer 0
	PCONP |= 1 << 2; 				// Habilitar Timer 1
	PCONP |= 1 << 22; 				// Habilitar Timer 2

	//PCLKSEL0 |= 1 << 2; 			// Clock for timer = CCLK Selecciono clock
	PCLKSEL0 |= 3 << 2; 			// Clock for timer0 = CCLK/8 Selecciono clock
	PCLKSEL0 |= 3 << 4; 			// Clock for timer1 = CCLK/8 Selecciono clock
	PCLKSEL0 |= 3 << 12; 			// Clock for timer2 = CCLK/8 Selecciono clock

	T0_MR0 = FULLpWM;
	T0_MR0I = 1;					// Configuro el match 0 para que interrumpa solamente
	T0_MR0R = 0;
	T0_MR0S = 0;

	T1_MR0 = FULLpWM;
	T1_MR0I = 1;					// Configuro el match 0 para que interrumpa solamente
	T1_MR0R = 0;
	T1_MR0S = 0;


	T2_MR0 = FULLpWM;
	T2_MR0I = 1;					// Configuro el match 0 para que interrumpa solamente
	T2_MR0R = 0;
	T2_MR0S = 0;

/*
	T0_TCR_CE = 0;					// Apago y reseteo el temporizador
	T0_TCR_CR = 1;

	T0_TCR_CR = 0;					// Enciendo el temporizador
	T0_TCR_CE = 1;

	T1_TCR_CE = 0;					// Apago y reseteo el temporizador
	T1_TCR_CR = 1;

	T1_TCR_CR = 0;					// Enciendo el temporizador
	T1_TCR_CE = 1;

	T2_TCR_CE = 0;					// Apago y reseteo el temporizador
	T2_TCR_CR = 1;

	T2_TCR_CR = 0;					// Enciendo el temporizador
	T2_TCR_CE = 1;
*/



	NVIC_HabilitarIRQ(TIMER0_IRQn); 	// Habilito interrupcion del Timer0 en el vector de interrupciones.
	NVIC_HabilitarIRQ(TIMER1_IRQn); 	// Habilito interrupcion del Timer1 en el vector de interrupciones.
	NVIC_HabilitarIRQ(TIMER2_IRQn); 	// Habilito interrupcion del Timer2 en el vector de interrupciones.


}

void InitPLL ( void )
{

	//Este bloque de codigo habilita el oscilador externo como fuente de clk
	//del micro, y configura un dispositivo conocido como PLL (Phase Locked Loop)
	//para generar un clock interno de 100MHz a partir del oscilador conectado

	SCS = SCS_Value;

	if (SCS_Value & (1 << 5))               /* If Main Oscillator is enabled      */
		while ((SCS & (1<<6)) == 0);/* Wait for Oscillator to be ready    */

	CCLKCFG   = CCLKCFG_Value;      /* Setup Clock Divider                */

	PCLKSEL0  = PCLKSEL0_Value;     /* Peripheral Clock Selection         */
	PCLKSEL1  = PCLKSEL1_Value;

	CLKSRCSEL = CLKSRCSEL_Value;    /* Select Clock Source for PLL0       */

	PLL0CFG   = PLL0CFG_Value;      /* configure PLL0                     */
	PLL0FEED  = 0xAA;
	PLL0FEED  = 0x55;

	PLL0CON   = 0x01;             /* PLL0 Enable                        */
	PLL0FEED  = 0xAA;
	PLL0FEED  = 0x55;

	while (!(PLL0STAT & (1<<26)));/* Wait for PLOCK0                    */

	PLL0CON   = 0x03;             /* PLL0 Enable & Connect              */
	PLL0FEED  = 0xAA;
	PLL0FEED  = 0x55;

	while (!(PLL0STAT & ((1<<25) | (1<<24))));/* Wait for PLLC0_STAT & PLLE0_STAT */

	PLL1CFG   = PLL1CFG_Value;
	PLL1FEED  = 0xAA;
	PLL1FEED  = 0x55;

	PLL1CON   = 0x01;             /* PLL1 Enable                        */
	PLL1FEED  = 0xAA;
	PLL1FEED  = 0x55;

	while (!(PLL1STAT & (1<<10)));/* Wait for PLOCK1                    */

	PLL1CON   = 0x03;             /* PLL1 Enable & Connect              */
	PLL1FEED  = 0xAA;
	PLL1FEED  = 0x55;

	while (!(PLL1STAT & ((1<< 9) | (1<< 8))));/* Wait for PLLC1_STAT & PLLE1_STAT */

	PCONP     = PCONP_Value;        /* Power Control for Peripherals      */

	CLKOUTCFG = CLKOUTCFG_Value;    /* Clock Output Configuration         */

	FLASHCFG  = (FLASHCFG & ~0x0000F000) | FLASHCFG_Value;
}
