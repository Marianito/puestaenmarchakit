/**
 	\file KitInfo2FW_Teclado.c
 	\brief Driver Driver de teclado
 	\details Expansion 4
 	\author Ing. Marcelo Trujillo
 	\date 2012.04.25
*/

#include "KitInfo2.h"

// Buffer de teclado
extern volatile unsigned char key;
extern volatile unsigned char keyFija;

void DriverTeclado(void)
{
	unsigned char CodigoActual ;
	CodigoActual = EX4_DriverTecladoHW( );

	DriverTecladoSW( CodigoActual );
}

void DriverTecladoFijo(void)
{
	unsigned char CodigoActual ;
	CodigoActual = DriverTecladoFijoHW( );

	DriverTecladoSWfijo( CodigoActual );
}

void DriverTecladoSW ( unsigned char CodigoActual )
{
	static unsigned char CodigoAnterior = NO_KEY;
	static unsigned char EstadosEstables;

	if( CodigoActual == NO_KEY )
	{
		CodigoAnterior = NO_KEY;
		EstadosEstables = 0;
		return;
	}

	if( EstadosEstables == 0  )
	{
		CodigoAnterior = CodigoActual;
		EstadosEstables = 1;
		return;
	}

	if( CodigoActual != CodigoAnterior )
	{
		CodigoAnterior = NO_KEY;
		EstadosEstables = 0;
		return;
	}

	if( EstadosEstables == REBOTES )
	{
		key = CodigoActual;
		EstadosEstables++;

		return;
	}

	if( EstadosEstables == REBOTES + 1)
		return;

	EstadosEstables ++;

	return;
}


void DriverTecladoSWfijo ( unsigned char CodigoActual )
{
	static unsigned char CodigoAnterior = NO_KEY;
	static unsigned char EstadosEstables;

	if( CodigoActual == NO_KEY )
	{
		CodigoAnterior = NO_KEY;
		EstadosEstables = 0;
		return;
	}

	if( EstadosEstables == 0  )
	{
		CodigoAnterior = CodigoActual;
		EstadosEstables = 1;
		return;
	}

	if( CodigoActual != CodigoAnterior )
	{
		CodigoAnterior = NO_KEY;
		EstadosEstables = 0;
		return;
	}

	if( EstadosEstables == REBOTES )
	{
		keyFija = CodigoActual;
		EstadosEstables++;

		return;
	}

	if( EstadosEstables == REBOTES + 1)
		return;

	EstadosEstables ++;

	return;
}

unsigned char EX4_DriverTecladoHW( void )
{
	unsigned char Codigo = NO_KEY;

	columna0 = OFF;
	columna1 = ON;
	columna2 = ON;
	columna0 = OFF;
	columna1 = ON;
	columna2 = ON;
	columna0 = OFF;
	columna1 = ON;
	columna2 = ON;
	columna0 = OFF;
	columna1 = ON;
	columna2 = ON;


	if ( !fila0 )
		return 0;

	if ( !fila1 )
		return 1;

	if ( !fila2 )
		return 2;

	columna0 = ON;
	columna1 = OFF;
	columna2 = ON;
	columna0 = ON;
	columna1 = OFF;
	columna2 = ON;
	columna0 = ON;
	columna1 = OFF;
	columna2 = ON;
	columna0 = ON;
	columna1 = OFF;
	columna2 = ON;
	columna0 = ON;
	columna1 = OFF;
	columna2 = ON;

	if ( !fila0 )
		return 3;

	if ( !fila1 )
		return 4;

	if ( !fila2 )
		return 5;

	columna0 = ON;
	columna1 = ON;
	columna2 = OFF;
	columna0 = ON;
	columna1 = ON;
	columna2 = OFF;
	columna0 = ON;
	columna1 = ON;
	columna2 = OFF;
	columna0 = ON;
	columna1 = ON;
	columna2 = OFF;
	columna0 = ON;
	columna1 = ON;
	columna2 = OFF;
	columna0 = ON;
	columna1 = ON;
	columna2 = OFF;

	if ( !fila0 )
		return 6;

	if ( !fila1 )
		return 7;

	if ( !fila2 )
		return 8;

	return Codigo;
}

unsigned char DriverTecladoFijoHW( void )
{
	unsigned char Codigo = NO_KEY;


	if ( !TECLAfIJA0 )
		return 0;

	if ( !TECLAfIJA1 )
		return 1;

	if ( !TECLAfIJA2 )
		return 2;

	if ( !TECLAfIJA3 )
		return 3;

	if ( !TECLAfIJA4 )
			return 4;

return Codigo;

}
