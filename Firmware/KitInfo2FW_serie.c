/*
 * KitInfo2FW_serie.c
 *
 *  Created on: 23/03/2014
 *      Author: Papuepes
 */
#include "KitInfo2.h"
extern volatile uint8_t TxStart;
// Buffer de Transmision
extern volatile uint8_t BufferTx[TXBUFFER_SIZE];
// Buffer de Recepcion
extern volatile uint8_t BufferRx[RXBUFFER_SIZE];

// Indices de Transmision
extern volatile uint8_t IndiceTxIn,IndiceTxOut;

// Indices de Recepcion
extern volatile uint8_t IndiceRxIn,IndiceRxOut;

extern volatile uint8_t TxStart;


void UART1_IRQHandler (void)
{
	int iir;
	unsigned char IntEnCurso , IntPendiente ;
	unsigned int dato ;

	do
	{  //IIR es reset por HW, una vez que lo lei se resetea.

		iir = U1IIR;

		IntPendiente = iir & 0x01;
		IntEnCurso = ( iir >> 1 ) & 0x07;

		switch( IntEnCurso )
		{
			case 2:	/* Receive Data Available */
				dato = U1RBR;		// Lectura del dato
				PushRx( (unsigned char ) dato );	// Guardo el dato
				break;

			case 1:	/* THRE, transmit holding register empty */

				dato = PopTx();						// Tomo el dato a Transmitir
				if ( dato != NO_KEY )
					U1THR = (unsigned char) dato;	// si hay dato en la cola lo Transmito
				else
					TxStart = 0;
				break;

		}
	}
	while( ! ( IntPendiente & 0x01 ) ); /* me fijo si cuando entr‚ a la ISR habia otra
						     	int. pendiente de atenci¢n: b0=1 (ocurre £nicamente si dentro del mismo
								espacio temporal lleguan dos interrupciones a la vez) */
}



