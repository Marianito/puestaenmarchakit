/**
 	\file KitInfo2FW_TimerTick.c
 	\brief Interrupcion del System Tick
 	\details Aplicacion para Expansion 4
 	\author Ing. Marcelo Trujillo
 	\date 2012.04.25
*/

#include "KitInfo2.h"

extern volatile uint32_t Ticks;

// Buffer de Salidas
extern volatile unsigned char BufferSalidas;
extern volatile unsigned char EX4_modo;
extern unsigned char BufferLeds;							//!< Variable de estado de las LEDS
extern volatile unsigned char BufferSalidas;				//!< Buffer de Salidas Relay
extern volatile unsigned char decimas;						//!< Base de tiempo decimas
extern volatile unsigned char segundos;
extern unsigned char flagTiempoAuxiliar, flagSeno;
extern unsigned int buffer_seno[ ];
extern volatile unsigned char flagPWMrojo, flagPWMverde, flagPWMazul;
extern volatile int bufferLedRGBrojo, bufferLedRGBverde, bufferLedRGBazul;
extern volatile int Demora_LCD;
extern volatile unsigned char flagLedsRGB;


void SysTick_Handler(void)
{
	DriverTeclado( );
	DriverTecladoFijo( );
	Dato_LCD( );
	EX4_BarridoMatriz( );

	Debounce( );

	relay0 =   BufferSalidas        & 0x01;
	relay1 = ( BufferSalidas >> 1 ) & 0x01;
	relay2 = ( BufferSalidas >> 2 ) & 0x01;
	relay3 = ( BufferSalidas >> 3 ) & 0x01;


	if( !flagLedsRGB )
	{
		led0 =   BufferLeds        & 0x01;
		led1 = ( BufferLeds >> 1 ) & 0x01;
		led2 = ( BufferLeds >> 2 ) & 0x01;
	}


	if ( Demora_LCD )
		Demora_LCD--;

	Ticks--	;

	if ( !Ticks )
	{
		switch ( EX4_modo )
		{
			case ROTAR_DERECHA:
				EX4_DspRotarDerecha( );
				break;
			case ROTAR_IZQUIERDA:
				EX4_DspRotarIzquierda( );
				break;
			case ROTAR_ABAJO:
				EX4_DspRotarAbajo( );
				break;
			case ROTAR_ARRIBA:
				EX4_DspRotarArriba( );
				break;
		}
		Ticks = 100;

		AnalizarTimer( TIMERlEDS );
		AnalizarTimer( TIMERrEFRESCOaDC );
		decimas --;
		if( !decimas )
			{
				decimas= DECIMAS;
				flagTiempoAuxiliar ++;
				flagTiempoAuxiliar %= 2;

			}


	}
}

void SysTickInic( void )
{
	STRELOAD  = ( STCALIB/4) - 1 ;      			//* set reload register

	STCURR = 0;                                   //* Load the SysTick Counter Value

	ENABLE = 1;
	TICKINT = 1;
	CLKSOURCE = 1;
	return ;
}

void TIMER0_IRQHandler (void)
{
	T0_IR_MR0 = 1;


        	if( flagPWMrojo )
        	    	{
        				flagPWMrojo = 0;
        				led0 = OFF;
        				T0_MR0 = (FULLpWM - bufferLedRGBrojo);

        	    	}
        	    	else
        			{
        				flagPWMrojo = 1;
        				if( bufferLedRGBrojo )
        					{
        					led0 = ON;

        					}
        				else
        					{
        					led0 = OFF;

        					}
        				T0_MR0 = (bufferLedRGBrojo);
        			}

        	T0_TCR_CR = 1;
       		T0_TCR_CR = 0;					// Enciendo el temporizador

}

void TIMER1_IRQHandler (void)
{
	T1_IR_MR0 = 1;


	if( flagPWMverde )
			{
				flagPWMverde = 0;
				led1 = OFF;
				T1_MR0 = (FULLpWM - bufferLedRGBverde);

			}
			else
			{
				flagPWMverde = 1;
				if( bufferLedRGBverde )
					{
					led1 = ON;

					}
				else
					{
					led1 = OFF;

					}
				T1_MR0 = (bufferLedRGBverde);
			}

	T1_TCR_CR = 1;
	T1_TCR_CR = 0;					// Enciendo el temporizador

}

void TIMER2_IRQHandler (void)
{
	T2_IR_MR0 = 1;


	if( flagPWMazul )
			{
				flagPWMazul = 0;
				led2 = OFF;
				T2_MR0 = (FULLpWM - bufferLedRGBazul);

			}
			else
			{
				flagPWMazul = 1;
				if( bufferLedRGBazul )
					{
					led2 = ON;

					}
				else
					{
					led2 = OFF;

					}
				T2_MR0 = (bufferLedRGBazul);
			}

	T2_TCR_CR = 1;
	T2_TCR_CR = 0;					// Enciendo el temporizador
}
