/**
 	\file KitInfo2FW_DisplayMatricial.c
 	\brief Drivers del display matricial
 	\details Valida Expansion 4
 	\author Ing. Marcelo Trujillo
 	\date 2012.04.25
*/
#include "KitInfo2.h"

extern struct matriz EX4_Caracteres[16][16];
extern volatile struct matriz EX4_BufferMatriz[];
extern volatile struct matriz EX4_BufferDisplay[];
extern volatile int EX4_lstring;

void EX4_DspRotarAbajo( void )
{
	int j , i ;
	for ( j = 0 ; j <  EX4_lstring ; j ++ )
	{
		for ( i = 7 ; i ; i-- )
			EX4_BufferDisplay[ j ].Fila[ i ] = EX4_BufferDisplay[ j ].Fila[ i -1 ] ;

		EX4_BufferDisplay[ j ].Fila[ 0 ] = EX4_BufferDisplay[ j ].Fila[ 7 ] ;
	}

	for ( i = 0 ; i < EX4_DIGITOS  ; i++ )
		EX4_BufferMatriz[ i ] = EX4_BufferDisplay[ EX4_DIGITOS - i - 1 ] ;
}

void EX4_DspRotarArriba( void )
{
	int j , i ;
	for ( j = 0 ; j <  EX4_lstring ; j++ )
	{
		EX4_BufferDisplay[ j ].Fila[ 7 ] = EX4_BufferDisplay[ j ].Fila[ 0 ] ;

		for ( i = 0 ; i < 7  ; i++ )
			EX4_BufferDisplay[ j ].Fila[ i ] = EX4_BufferDisplay[ j ].Fila[ i + 1 ] ;
	}

	for ( i = 0 ; i < EX4_DIGITOS  ; i++ )
		EX4_BufferMatriz[ i ] = EX4_BufferDisplay[ EX4_DIGITOS - i - 1 ] ;
}

void EX4_DspRotarIzquierda( void )
{
	int j , i ;

	for ( j = 0 ; j <  EX4_lstring ; j ++ )
	{
		for ( i = 0 ; i < 7 ; i++ )
			EX4_BufferDisplay[ j ].Fila[ i ] <<= 1 ;

		if ( j < EX4_lstring - 1 )
		{
			for ( i = 0 ; i < 7 ; i++ )
				EX4_BufferDisplay[ j ].Fila[ i ] |= ( ( EX4_BufferDisplay[ j + 1 ].Fila[ i ] >> 4 ) & 0x01 ) ;
		}
		else
		{
			for ( i = 0 ; i < 7 ; i++ )
				EX4_BufferDisplay[ j ].Fila[ i ] |= (( EX4_BufferDisplay[ 0 ].Fila[i] >> 5 ) & 0x01 );
		}
	}

	for ( i = 0 ; i < EX4_DIGITOS  ; i++ )
		EX4_BufferMatriz[ i ] = EX4_BufferDisplay[ EX4_DIGITOS - i - 1 ] ;
}

void EX4_DspRotarDerecha( void )
{
	int j , i ;
	unsigned char b[ 8 ] ;

	for ( i = 0 ; i < 7 ; i++ )
		b[ i ] = ( ( EX4_BufferDisplay[ EX4_lstring -1 ].Fila[ i ] << 5 ) & 0x20 ) ;

	for (j = 0 ; j <  EX4_lstring ; j++ )
	{
		for ( i = 0 ; i < 7 ; i ++ )
		{
			EX4_BufferDisplay[ j ].Fila[ i ] &= 0xdf ;
			EX4_BufferDisplay[ j ].Fila[ i ] |= b[i] ;
		}

		for ( i = 0 ; i < 7 ; i ++ )
			b[ i ] = ((EX4_BufferDisplay[ j ].Fila[ i ] << 5 ) & 0x20 ) ;

		for ( i = 0 ; i < 7 ; i++ )
			EX4_BufferDisplay[ j ].Fila[ i ] >>= 1 ;
	}
	for ( i = 0 ; i < EX4_DIGITOS  ; i++ )
		EX4_BufferMatriz[ i ] = EX4_BufferDisplay[ EX4_DIGITOS - i - 1 ] ;
}

void EX4_ShiftDato( int fila )
{
	int i , j ;

	for ( j = 0 ; j < EX4_DIGITOS ; j++ )
	{
		for ( i = 0 ; i < 5 ; i++ )
		{
			EX4_Data = ( EX4_BufferMatriz[ j ].Fila[fila] >> i ) & 1 ;

			EX4_Clock = ON ;
			EX4_Clock = OFF ;
		}
	}
	
	EX4_Output = ON ;
	EX4_Output = OFF ;

	return ;
}

void EX4_BarridoMatriz( void )
{
	static int	fila = 0 ;

	EX4_M_fila0 = OFF ; 
	EX4_M_fila1 = OFF ;
	EX4_M_fila2 = OFF ;
	EX4_M_fila3 = OFF ;
	EX4_M_fila4 = OFF ;
	EX4_M_fila5 = OFF ;
	EX4_M_fila6 = OFF ;

	EX4_ShiftDato( fila ) ;

	switch( fila )
	{
		case EX4_M_FILA0: EX4_M_fila0 = ON ; break ;
		case EX4_M_FILA1: EX4_M_fila1 = ON ; break ;
		case EX4_M_FILA2: EX4_M_fila2 = ON ; break ;
		case EX4_M_FILA3: EX4_M_fila3 = ON ; break ;
		case EX4_M_FILA4: EX4_M_fila4 = ON ; break ;
		case EX4_M_FILA5: EX4_M_fila5 = ON ; break ;
		case EX4_M_FILA6: EX4_M_fila6 = ON ; break ;
	}

	fila++ ;
	fila %= 7 ;
}



void EX4_TablaCaracteres(void)
{
      // Digito # : -----------------------------------------------
      EX4_Caracteres[0x00][0x00].Fila[0] = 0x00 ;
      EX4_Caracteres[0x00][0x00].Fila[1] = 0x00 ;
      EX4_Caracteres[0x00][0x00].Fila[2] = 0x00 ;
      EX4_Caracteres[0x00][0x00].Fila[3] = 0x00 ;
      EX4_Caracteres[0x00][0x00].Fila[4] = 0x00 ;
      EX4_Caracteres[0x00][0x00].Fila[5] = 0x00 ;
      EX4_Caracteres[0x00][0x00].Fila[6] = 0x00 ;
      // Digito ! : -----------------------------------------------
      EX4_Caracteres[0x02][0x01].Fila[0] = 0x04 ;
      EX4_Caracteres[0x02][0x01].Fila[1] = 0x04 ;
      EX4_Caracteres[0x02][0x01].Fila[2] = 0x04 ;
      EX4_Caracteres[0x02][0x01].Fila[3] = 0x04 ;
      EX4_Caracteres[0x02][0x01].Fila[4] = 0x04 ;
      EX4_Caracteres[0x02][0x01].Fila[5] = 0x00 ;
      EX4_Caracteres[0x02][0x01].Fila[6] = 0x04 ;
      // Digito " : -----------------------------------------------
      EX4_Caracteres[0x02][0x02].Fila[0] = 0x06 ;
      EX4_Caracteres[0x02][0x02].Fila[1] = 0x06 ;
      EX4_Caracteres[0x02][0x02].Fila[2] = 0x06 ;
      EX4_Caracteres[0x02][0x02].Fila[3] = 0x00 ;
      EX4_Caracteres[0x02][0x02].Fila[4] = 0x00 ;
      EX4_Caracteres[0x02][0x02].Fila[5] = 0x00 ;
      EX4_Caracteres[0x02][0x02].Fila[6] = 0x00 ;
      // Digito # : -----------------------------------------------
      EX4_Caracteres[0x02][0x03].Fila[0] = 0x0a ;
      EX4_Caracteres[0x02][0x03].Fila[1] = 0x0a ;
      EX4_Caracteres[0x02][0x03].Fila[2] = 0x1f ;
      EX4_Caracteres[0x02][0x03].Fila[3] = 0x0a ;
      EX4_Caracteres[0x02][0x03].Fila[4] = 0x1f ;
      EX4_Caracteres[0x02][0x03].Fila[5] = 0x0a ;
      EX4_Caracteres[0x02][0x03].Fila[6] = 0x0a ;
      // Digito $ : -----------------------------------------------
      EX4_Caracteres[0x02][0x04].Fila[0] = 0x04 ;
      EX4_Caracteres[0x02][0x04].Fila[1] = 0x0f ;
      EX4_Caracteres[0x02][0x04].Fila[2] = 0x14 ;
      EX4_Caracteres[0x02][0x04].Fila[3] = 0x0e ;
      EX4_Caracteres[0x02][0x04].Fila[4] = 0x05 ;
      EX4_Caracteres[0x02][0x04].Fila[5] = 0x1e ;
      EX4_Caracteres[0x02][0x04].Fila[6] = 0x04 ;
      // Digito % : -----------------------------------------------
      EX4_Caracteres[0x02][0x05].Fila[0] = 0x18 ;
      EX4_Caracteres[0x02][0x05].Fila[1] = 0x19 ;
      EX4_Caracteres[0x02][0x05].Fila[2] = 0x02 ;
      EX4_Caracteres[0x02][0x05].Fila[3] = 0x04 ;
      EX4_Caracteres[0x02][0x05].Fila[4] = 0x08 ;
      EX4_Caracteres[0x02][0x05].Fila[5] = 0x13 ;
      EX4_Caracteres[0x02][0x05].Fila[6] = 0x03 ;
      // Digito & : -----------------------------------------------
      EX4_Caracteres[0x02][0x06].Fila[0] = 0x0c ;
      EX4_Caracteres[0x02][0x06].Fila[1] = 0x12 ;
      EX4_Caracteres[0x02][0x06].Fila[2] = 0x14 ;
      EX4_Caracteres[0x02][0x06].Fila[3] = 0x08 ;
      EX4_Caracteres[0x02][0x06].Fila[4] = 0x15 ;
      EX4_Caracteres[0x02][0x06].Fila[5] = 0x12 ;
      EX4_Caracteres[0x02][0x06].Fila[6] = 0x0d ;
      // Digito ' : -----------------------------------------------
      EX4_Caracteres[0x02][0x07].Fila[0] = 0x06 ;
      EX4_Caracteres[0x02][0x07].Fila[1] = 0x02 ;
      EX4_Caracteres[0x02][0x07].Fila[2] = 0x04 ;
      EX4_Caracteres[0x02][0x07].Fila[3] = 0x00 ;
      EX4_Caracteres[0x02][0x07].Fila[4] = 0x00 ;
      EX4_Caracteres[0x02][0x07].Fila[5] = 0x00 ;
      EX4_Caracteres[0x02][0x07].Fila[6] = 0x00 ;
      // Digito ( : -----------------------------------------------
      EX4_Caracteres[0x02][0x08].Fila[0] = 0x02 ;
      EX4_Caracteres[0x02][0x08].Fila[1] = 0x04 ;
      EX4_Caracteres[0x02][0x08].Fila[2] = 0x08 ;
      EX4_Caracteres[0x02][0x08].Fila[3] = 0x08 ;
      EX4_Caracteres[0x02][0x08].Fila[4] = 0x08 ;
      EX4_Caracteres[0x02][0x08].Fila[5] = 0x04 ;
      EX4_Caracteres[0x02][0x08].Fila[6] = 0x02 ;
      // Digito ) : -----------------------------------------------
      EX4_Caracteres[0x02][0x09].Fila[0] = 0x08 ;
      EX4_Caracteres[0x02][0x09].Fila[1] = 0x04 ;
      EX4_Caracteres[0x02][0x09].Fila[2] = 0x02 ;
      EX4_Caracteres[0x02][0x09].Fila[3] = 0x02 ;
      EX4_Caracteres[0x02][0x09].Fila[4] = 0x02 ;
      EX4_Caracteres[0x02][0x09].Fila[5] = 0x04 ;
      EX4_Caracteres[0x02][0x09].Fila[6] = 0x08 ;
      // Digito * : -----------------------------------------------
      EX4_Caracteres[0x02][0x0a].Fila[0] = 0x00 ;
      EX4_Caracteres[0x02][0x0a].Fila[1] = 0x04 ;
      EX4_Caracteres[0x02][0x0a].Fila[2] = 0x15 ;
      EX4_Caracteres[0x02][0x0a].Fila[3] = 0x0e ;
      EX4_Caracteres[0x02][0x0a].Fila[4] = 0x15 ;
      EX4_Caracteres[0x02][0x0a].Fila[5] = 0x04 ;
      EX4_Caracteres[0x02][0x0a].Fila[6] = 0x00 ;
      // Digito + : -----------------------------------------------
      EX4_Caracteres[0x02][0x0b].Fila[0] = 0x00 ;
      EX4_Caracteres[0x02][0x0b].Fila[1] = 0x04 ;
      EX4_Caracteres[0x02][0x0b].Fila[2] = 0x04 ;
      EX4_Caracteres[0x02][0x0b].Fila[3] = 0x1f ;
      EX4_Caracteres[0x02][0x0b].Fila[4] = 0x04 ;
      EX4_Caracteres[0x02][0x0b].Fila[5] = 0x04 ;
      EX4_Caracteres[0x02][0x0b].Fila[6] = 0x00 ;
      // Digito , : -----------------------------------------------
      EX4_Caracteres[0x02][0x0c].Fila[0] = 0x00 ;
      EX4_Caracteres[0x02][0x0c].Fila[1] = 0x00 ;
      EX4_Caracteres[0x02][0x0c].Fila[2] = 0x00 ;
      EX4_Caracteres[0x02][0x0c].Fila[3] = 0x00 ;
      EX4_Caracteres[0x02][0x0c].Fila[4] = 0x06 ;
      EX4_Caracteres[0x02][0x0c].Fila[5] = 0x02 ;
      EX4_Caracteres[0x02][0x0c].Fila[6] = 0x04 ;
      // Digito - : -----------------------------------------------
      EX4_Caracteres[0x02][0x0d].Fila[0] = 0x00 ;
      EX4_Caracteres[0x02][0x0d].Fila[1] = 0x00 ;
      EX4_Caracteres[0x02][0x0d].Fila[2] = 0x00 ;
      EX4_Caracteres[0x02][0x0d].Fila[3] = 0x1f ;
      EX4_Caracteres[0x02][0x0d].Fila[4] = 0x00 ;
      EX4_Caracteres[0x02][0x0d].Fila[5] = 0x00 ;
      EX4_Caracteres[0x02][0x0d].Fila[6] = 0x00 ;
      // Digito . : -----------------------------------------------
      EX4_Caracteres[0x02][0x0e].Fila[0] = 0x00 ;
      EX4_Caracteres[0x02][0x0e].Fila[1] = 0x00 ;
      EX4_Caracteres[0x02][0x0e].Fila[2] = 0x00 ;
      EX4_Caracteres[0x02][0x0e].Fila[3] = 0x00 ;
      EX4_Caracteres[0x02][0x0e].Fila[4] = 0x00 ;
      EX4_Caracteres[0x02][0x0e].Fila[5] = 0x06 ;
      EX4_Caracteres[0x02][0x0e].Fila[6] = 0x06 ;
      // Digito / : -----------------------------------------------
      EX4_Caracteres[0x02][0x0f].Fila[0] = 0x00 ;
      EX4_Caracteres[0x02][0x0f].Fila[1] = 0x01 ;
      EX4_Caracteres[0x02][0x0f].Fila[2] = 0x02 ;
      EX4_Caracteres[0x02][0x0f].Fila[3] = 0x04 ;
      EX4_Caracteres[0x02][0x0f].Fila[4] = 0x08 ;
      EX4_Caracteres[0x02][0x0f].Fila[5] = 0x10 ;
      EX4_Caracteres[0x02][0x0f].Fila[6] = 0x00 ;
      // Digito @ : -----------------------------------------------
      EX4_Caracteres[0x04][0x00].Fila[0] = 0x0e ;
      EX4_Caracteres[0x04][0x00].Fila[1] = 0x11 ;
      EX4_Caracteres[0x04][0x00].Fila[2] = 0x01 ;
      EX4_Caracteres[0x04][0x00].Fila[3] = 0x0d ;
      EX4_Caracteres[0x04][0x00].Fila[4] = 0x15 ;
      EX4_Caracteres[0x04][0x00].Fila[5] = 0x15 ;
      EX4_Caracteres[0x04][0x00].Fila[6] = 0x0e ;
      // Digito A : -----------------------------------------------
      EX4_Caracteres[0x04][0x01].Fila[0] = 0x06 ;
      EX4_Caracteres[0x04][0x01].Fila[1] = 0x09 ;
      EX4_Caracteres[0x04][0x01].Fila[2] = 0x09 ;
      EX4_Caracteres[0x04][0x01].Fila[3] = 0x0f ;
      EX4_Caracteres[0x04][0x01].Fila[4] = 0x09 ;
      EX4_Caracteres[0x04][0x01].Fila[5] = 0x09 ;
      EX4_Caracteres[0x04][0x01].Fila[6] = 0x09 ;
      // Digito B : -----------------------------------------------
      EX4_Caracteres[0x04][0x02].Fila[0] = 0x0a ;
      EX4_Caracteres[0x04][0x02].Fila[1] = 0x09 ;
      EX4_Caracteres[0x04][0x02].Fila[2] = 0x09 ;
      EX4_Caracteres[0x04][0x02].Fila[3] = 0x0e ;
      EX4_Caracteres[0x04][0x02].Fila[4] = 0x09 ;
      EX4_Caracteres[0x04][0x02].Fila[5] = 0x09 ;
      EX4_Caracteres[0x04][0x02].Fila[6] = 0x0e ;
      // Digito C : -----------------------------------------------
      EX4_Caracteres[0x04][0x03].Fila[0] = 0x06 ;
      EX4_Caracteres[0x04][0x03].Fila[1] = 0x09 ;
      EX4_Caracteres[0x04][0x03].Fila[2] = 0x08 ;
      EX4_Caracteres[0x04][0x03].Fila[3] = 0x08 ;
      EX4_Caracteres[0x04][0x03].Fila[4] = 0x08 ;
      EX4_Caracteres[0x04][0x03].Fila[5] = 0x09 ;
      EX4_Caracteres[0x04][0x03].Fila[6] = 0x06 ;
      // Digito D : -----------------------------------------------
      EX4_Caracteres[0x04][0x04].Fila[0] = 0x0e ;
      EX4_Caracteres[0x04][0x04].Fila[1] = 0x09 ;
      EX4_Caracteres[0x04][0x04].Fila[2] = 0x09 ;
      EX4_Caracteres[0x04][0x04].Fila[3] = 0x09 ;
      EX4_Caracteres[0x04][0x04].Fila[4] = 0x09 ;
      EX4_Caracteres[0x04][0x04].Fila[5] = 0x09 ;
      EX4_Caracteres[0x04][0x04].Fila[6] = 0x0e ;
      // Digito E : -----------------------------------------------
      EX4_Caracteres[0x04][0x05].Fila[0] = 0x0f ;
      EX4_Caracteres[0x04][0x05].Fila[1] = 0x08 ;
      EX4_Caracteres[0x04][0x05].Fila[2] = 0x08 ;
      EX4_Caracteres[0x04][0x05].Fila[3] = 0x0e ;
      EX4_Caracteres[0x04][0x05].Fila[4] = 0x08 ;
      EX4_Caracteres[0x04][0x05].Fila[5] = 0x08 ;
      EX4_Caracteres[0x04][0x05].Fila[6] = 0x0f ;
      // Digito F : -----------------------------------------------
      EX4_Caracteres[0x04][0x06].Fila[0] = 0x0f ;
      EX4_Caracteres[0x04][0x06].Fila[1] = 0x08 ;
      EX4_Caracteres[0x04][0x06].Fila[2] = 0x08 ;
      EX4_Caracteres[0x04][0x06].Fila[3] = 0x0e ;
      EX4_Caracteres[0x04][0x06].Fila[4] = 0x08 ;
      EX4_Caracteres[0x04][0x06].Fila[5] = 0x08 ;
      EX4_Caracteres[0x04][0x06].Fila[6] = 0x08 ;
      // Digito G : -----------------------------------------------
      EX4_Caracteres[0x04][0x07].Fila[0] = 0x06 ;
      EX4_Caracteres[0x04][0x07].Fila[1] = 0x09 ;
      EX4_Caracteres[0x04][0x07].Fila[2] = 0x08 ;
      EX4_Caracteres[0x04][0x07].Fila[3] = 0x0b ;
      EX4_Caracteres[0x04][0x07].Fila[4] = 0x09 ;
      EX4_Caracteres[0x04][0x07].Fila[5] = 0x09 ;
      EX4_Caracteres[0x04][0x07].Fila[6] = 0x07 ;
      // Digito H : -----------------------------------------------
      EX4_Caracteres[0x04][0x08].Fila[0] = 0x09 ;
      EX4_Caracteres[0x04][0x08].Fila[1] = 0x09 ;
      EX4_Caracteres[0x04][0x08].Fila[2] = 0x09 ;
      EX4_Caracteres[0x04][0x08].Fila[3] = 0x0f ;
      EX4_Caracteres[0x04][0x08].Fila[4] = 0x09 ;
      EX4_Caracteres[0x04][0x08].Fila[5] = 0x09 ;
      EX4_Caracteres[0x04][0x08].Fila[6] = 0x09 ;
      // Digito I : -----------------------------------------------
      EX4_Caracteres[0x04][0x09].Fila[0] = 0x0e ;
      EX4_Caracteres[0x04][0x09].Fila[1] = 0x04 ;
      EX4_Caracteres[0x04][0x09].Fila[2] = 0x04 ;
      EX4_Caracteres[0x04][0x09].Fila[3] = 0x04 ;
      EX4_Caracteres[0x04][0x09].Fila[4] = 0x04 ;
      EX4_Caracteres[0x04][0x09].Fila[5] = 0x04 ;
      EX4_Caracteres[0x04][0x09].Fila[6] = 0x0e ;
      // Digito J : -----------------------------------------------
      EX4_Caracteres[0x04][0x0a].Fila[0] = 0x01 ;
      EX4_Caracteres[0x04][0x0a].Fila[1] = 0x01 ;
      EX4_Caracteres[0x04][0x0a].Fila[2] = 0x01 ;
      EX4_Caracteres[0x04][0x0a].Fila[3] = 0x01 ;
      EX4_Caracteres[0x04][0x0a].Fila[4] = 0x01 ;
      EX4_Caracteres[0x04][0x0a].Fila[5] = 0x09 ;
      EX4_Caracteres[0x04][0x0a].Fila[6] = 0x06 ;
      // Digito K : -----------------------------------------------
      EX4_Caracteres[0x04][0x0b].Fila[0] = 0x09 ;
      EX4_Caracteres[0x04][0x0b].Fila[1] = 0x0a ;
      EX4_Caracteres[0x04][0x0b].Fila[2] = 0x0c ;
      EX4_Caracteres[0x04][0x0b].Fila[3] = 0x0a ;
      EX4_Caracteres[0x04][0x0b].Fila[4] = 0x09 ;
      EX4_Caracteres[0x04][0x0b].Fila[5] = 0x09 ;
      EX4_Caracteres[0x04][0x0b].Fila[6] = 0x09 ;
      // Digito L : -----------------------------------------------
      EX4_Caracteres[0x04][0x0c].Fila[0] = 0x08 ;
      EX4_Caracteres[0x04][0x0c].Fila[1] = 0x08 ;
      EX4_Caracteres[0x04][0x0c].Fila[2] = 0x08 ;
      EX4_Caracteres[0x04][0x0c].Fila[3] = 0x08 ;
      EX4_Caracteres[0x04][0x0c].Fila[4] = 0x08 ;
      EX4_Caracteres[0x04][0x0c].Fila[5] = 0x08 ;
      EX4_Caracteres[0x04][0x0c].Fila[6] = 0x0f ;
      // Digito M : -----------------------------------------------
      EX4_Caracteres[0x04][0x0d].Fila[0] = 0x11 ;
      EX4_Caracteres[0x04][0x0d].Fila[1] = 0x1b ;
      EX4_Caracteres[0x04][0x0d].Fila[2] = 0x15 ;
      EX4_Caracteres[0x04][0x0d].Fila[3] = 0x11 ;
      EX4_Caracteres[0x04][0x0d].Fila[4] = 0x11 ;
      EX4_Caracteres[0x04][0x0d].Fila[5] = 0x11 ;
      EX4_Caracteres[0x04][0x0d].Fila[6] = 0x11 ;
      // Digito N : -----------------------------------------------
      EX4_Caracteres[0x04][0x0e].Fila[0] = 0x09 ;
      EX4_Caracteres[0x04][0x0e].Fila[1] = 0x0d ;
      EX4_Caracteres[0x04][0x0e].Fila[2] = 0x0b ;
      EX4_Caracteres[0x04][0x0e].Fila[3] = 0x09 ;
      EX4_Caracteres[0x04][0x0e].Fila[4] = 0x09 ;
      EX4_Caracteres[0x04][0x0e].Fila[5] = 0x09 ;
      EX4_Caracteres[0x04][0x0e].Fila[6] = 0x09 ;
      // Digito O : -----------------------------------------------
      EX4_Caracteres[0x04][0x0f].Fila[0] = 0x06 ;
      EX4_Caracteres[0x04][0x0f].Fila[1] = 0x09 ;
      EX4_Caracteres[0x04][0x0f].Fila[2] = 0x09 ;
      EX4_Caracteres[0x04][0x0f].Fila[3] = 0x09 ;
      EX4_Caracteres[0x04][0x0f].Fila[4] = 0x09 ;
      EX4_Caracteres[0x04][0x0f].Fila[5] = 0x09 ;
      EX4_Caracteres[0x04][0x0f].Fila[6] = 0x06 ;
      // Digito P : -----------------------------------------------
      EX4_Caracteres[0x05][0x00].Fila[0] = 0x0e ;
      EX4_Caracteres[0x05][0x00].Fila[1] = 0x09 ;
      EX4_Caracteres[0x05][0x00].Fila[2] = 0x09 ;
      EX4_Caracteres[0x05][0x00].Fila[3] = 0x0e ;
      EX4_Caracteres[0x05][0x00].Fila[4] = 0x08 ;
      EX4_Caracteres[0x05][0x00].Fila[5] = 0x08 ;
      EX4_Caracteres[0x05][0x00].Fila[6] = 0x08 ;
      // Digito Q : -----------------------------------------------
      EX4_Caracteres[0x05][0x01].Fila[0] = 0x0e ;
      EX4_Caracteres[0x05][0x01].Fila[1] = 0x11 ;
      EX4_Caracteres[0x05][0x01].Fila[2] = 0x11 ;
      EX4_Caracteres[0x05][0x01].Fila[3] = 0x11 ;
      EX4_Caracteres[0x05][0x01].Fila[4] = 0x15 ;
      EX4_Caracteres[0x05][0x01].Fila[5] = 0x12 ;
      EX4_Caracteres[0x05][0x01].Fila[6] = 0x0d ;
      // Digito R : -----------------------------------------------
      EX4_Caracteres[0x05][0x02].Fila[0] = 0x0e ;
      EX4_Caracteres[0x05][0x02].Fila[1] = 0x09 ;
      EX4_Caracteres[0x05][0x02].Fila[2] = 0x09 ;
      EX4_Caracteres[0x05][0x02].Fila[3] = 0x0e ;
      EX4_Caracteres[0x05][0x02].Fila[4] = 0x0a ;
      EX4_Caracteres[0x05][0x02].Fila[5] = 0x09 ;
      EX4_Caracteres[0x05][0x02].Fila[6] = 0x09 ;
      // Digito S : -----------------------------------------------
      EX4_Caracteres[0x05][0x03].Fila[0] = 0x07 ;
      EX4_Caracteres[0x05][0x03].Fila[1] = 0x08 ;
      EX4_Caracteres[0x05][0x03].Fila[2] = 0x08 ;
      EX4_Caracteres[0x05][0x03].Fila[3] = 0x06 ;
      EX4_Caracteres[0x05][0x03].Fila[4] = 0x01 ;
      EX4_Caracteres[0x05][0x03].Fila[5] = 0x01 ;
      EX4_Caracteres[0x05][0x03].Fila[6] = 0x0e ;
      // Digito T : -----------------------------------------------
      EX4_Caracteres[0x05][0x04].Fila[0] = 0x1f ;
      EX4_Caracteres[0x05][0x04].Fila[1] = 0x04 ;
      EX4_Caracteres[0x05][0x04].Fila[2] = 0x04 ;
      EX4_Caracteres[0x05][0x04].Fila[3] = 0x04 ;
      EX4_Caracteres[0x05][0x04].Fila[4] = 0x04 ;
      EX4_Caracteres[0x05][0x04].Fila[5] = 0x04 ;
      EX4_Caracteres[0x05][0x04].Fila[6] = 0x04 ;
      // Digito U : -----------------------------------------------
      EX4_Caracteres[0x05][0x05].Fila[0] = 0x09 ;
      EX4_Caracteres[0x05][0x05].Fila[1] = 0x09 ;
      EX4_Caracteres[0x05][0x05].Fila[2] = 0x09 ;
      EX4_Caracteres[0x05][0x05].Fila[3] = 0x09 ;
      EX4_Caracteres[0x05][0x05].Fila[4] = 0x09 ;
      EX4_Caracteres[0x05][0x05].Fila[5] = 0x09 ;
      EX4_Caracteres[0x05][0x05].Fila[6] = 0x06 ;
      // Digito V : -----------------------------------------------
      EX4_Caracteres[0x05][0x06].Fila[0] = 0x11 ;
      EX4_Caracteres[0x05][0x06].Fila[1] = 0x11 ;
      EX4_Caracteres[0x05][0x06].Fila[2] = 0x11 ;
      EX4_Caracteres[0x05][0x06].Fila[3] = 0x11 ;
      EX4_Caracteres[0x05][0x06].Fila[4] = 0x11 ;
      EX4_Caracteres[0x05][0x06].Fila[5] = 0x0a ;
      EX4_Caracteres[0x05][0x06].Fila[6] = 0x04 ;
      // Digito W : -----------------------------------------------
      EX4_Caracteres[0x05][0x07].Fila[0] = 0x11 ;
      EX4_Caracteres[0x05][0x07].Fila[1] = 0x11 ;
      EX4_Caracteres[0x05][0x07].Fila[2] = 0x11 ;
      EX4_Caracteres[0x05][0x07].Fila[3] = 0x11 ;
      EX4_Caracteres[0x05][0x07].Fila[4] = 0x15 ;
      EX4_Caracteres[0x05][0x07].Fila[5] = 0x15 ;
      EX4_Caracteres[0x05][0x07].Fila[6] = 0x0a ;
      // Digito X : -----------------------------------------------
      EX4_Caracteres[0x05][0x08].Fila[0] = 0x11 ;
      EX4_Caracteres[0x05][0x08].Fila[1] = 0x11 ;
      EX4_Caracteres[0x05][0x08].Fila[2] = 0x0a ;
      EX4_Caracteres[0x05][0x08].Fila[3] = 0x04 ;
      EX4_Caracteres[0x05][0x08].Fila[4] = 0x0a ;
      EX4_Caracteres[0x05][0x08].Fila[5] = 0x11 ;
      EX4_Caracteres[0x05][0x08].Fila[6] = 0x11 ;
      // Digito Y : -----------------------------------------------
      EX4_Caracteres[0x05][0x09].Fila[0] = 0x11 ;
      EX4_Caracteres[0x05][0x09].Fila[1] = 0x11 ;
      EX4_Caracteres[0x05][0x09].Fila[2] = 0x0a ;
      EX4_Caracteres[0x05][0x09].Fila[3] = 0x04 ;
      EX4_Caracteres[0x05][0x09].Fila[4] = 0x04 ;
      EX4_Caracteres[0x05][0x09].Fila[5] = 0x04 ;
      EX4_Caracteres[0x05][0x09].Fila[6] = 0x04 ;
      // Digito Z : -----------------------------------------------
      EX4_Caracteres[0x05][0x0a].Fila[0] = 0x1f ;
      EX4_Caracteres[0x05][0x0a].Fila[1] = 0x01 ;
      EX4_Caracteres[0x05][0x0a].Fila[2] = 0x02 ;
      EX4_Caracteres[0x05][0x0a].Fila[3] = 0x04 ;
      EX4_Caracteres[0x05][0x0a].Fila[4] = 0x08 ;
      EX4_Caracteres[0x05][0x0a].Fila[5] = 0x10 ;
      EX4_Caracteres[0x05][0x0a].Fila[6] = 0x1f ;
      // Digito [ : -----------------------------------------------
      EX4_Caracteres[0x05][0x0b].Fila[0] = 0x07 ;
      EX4_Caracteres[0x05][0x0b].Fila[1] = 0x04 ;
      EX4_Caracteres[0x05][0x0b].Fila[2] = 0x04 ;
      EX4_Caracteres[0x05][0x0b].Fila[3] = 0x04 ;
      EX4_Caracteres[0x05][0x0b].Fila[4] = 0x04 ;
      EX4_Caracteres[0x05][0x0b].Fila[5] = 0x04 ;
      EX4_Caracteres[0x05][0x0b].Fila[6] = 0x07 ;
      // Digito ] : -----------------------------------------------
      EX4_Caracteres[0x05][0x0d].Fila[0] = 0x1c ;
      EX4_Caracteres[0x05][0x0d].Fila[1] = 0x04 ;
      EX4_Caracteres[0x05][0x0d].Fila[2] = 0x04 ;
      EX4_Caracteres[0x05][0x0d].Fila[3] = 0x04 ;
      EX4_Caracteres[0x05][0x0d].Fila[4] = 0x04 ;
      EX4_Caracteres[0x05][0x0d].Fila[5] = 0x04 ;
      EX4_Caracteres[0x05][0x0d].Fila[6] = 0x1c ;
      // Digito _ : -----------------------------------------------
      EX4_Caracteres[0x05][0x0f].Fila[0] = 0x00 ;
      EX4_Caracteres[0x05][0x0f].Fila[1] = 0x00 ;
      EX4_Caracteres[0x05][0x0f].Fila[2] = 0x00 ;
      EX4_Caracteres[0x05][0x0f].Fila[3] = 0x00 ;
      EX4_Caracteres[0x05][0x0f].Fila[4] = 0x00 ;
      EX4_Caracteres[0x05][0x0f].Fila[5] = 0x00 ;
      EX4_Caracteres[0x05][0x0f].Fila[6] = 0x1f ;
      // Digito a : -----------------------------------------------
      EX4_Caracteres[0x06][0x01].Fila[0] = 0x00 ;
      EX4_Caracteres[0x06][0x01].Fila[1] = 0x00 ;
      EX4_Caracteres[0x06][0x01].Fila[2] = 0x06 ;
      EX4_Caracteres[0x06][0x01].Fila[3] = 0x01 ;
      EX4_Caracteres[0x06][0x01].Fila[4] = 0x07 ;
      EX4_Caracteres[0x06][0x01].Fila[5] = 0x09 ;
      EX4_Caracteres[0x06][0x01].Fila[6] = 0x07 ;
      // Digito b : -----------------------------------------------
      EX4_Caracteres[0x06][0x02].Fila[0] = 0x08 ;
      EX4_Caracteres[0x06][0x02].Fila[1] = 0x08 ;
      EX4_Caracteres[0x06][0x02].Fila[2] = 0x0a ;
      EX4_Caracteres[0x06][0x02].Fila[3] = 0x0d ;
      EX4_Caracteres[0x06][0x02].Fila[4] = 0x09 ;
      EX4_Caracteres[0x06][0x02].Fila[5] = 0x09 ;
      EX4_Caracteres[0x06][0x02].Fila[6] = 0x0e ;
      // Digito c : -----------------------------------------------
      EX4_Caracteres[0x06][0x03].Fila[0] = 0x00 ;
      EX4_Caracteres[0x06][0x03].Fila[1] = 0x00 ;
      EX4_Caracteres[0x06][0x03].Fila[2] = 0x06 ;
      EX4_Caracteres[0x06][0x03].Fila[3] = 0x08 ;
      EX4_Caracteres[0x06][0x03].Fila[4] = 0x08 ;
      EX4_Caracteres[0x06][0x03].Fila[5] = 0x09 ;
      EX4_Caracteres[0x06][0x03].Fila[6] = 0x06 ;
      // Digito d : -----------------------------------------------
      EX4_Caracteres[0x06][0x04].Fila[0] = 0x01 ;
      EX4_Caracteres[0x06][0x04].Fila[1] = 0x01 ;
      EX4_Caracteres[0x06][0x04].Fila[2] = 0x05 ;
      EX4_Caracteres[0x06][0x04].Fila[3] = 0x0b ;
      EX4_Caracteres[0x06][0x04].Fila[4] = 0x09 ;
      EX4_Caracteres[0x06][0x04].Fila[5] = 0x09 ;
      EX4_Caracteres[0x06][0x04].Fila[6] = 0x07 ;
      // Digito e : -----------------------------------------------
      EX4_Caracteres[0x06][0x05].Fila[0] = 0x00 ;
      EX4_Caracteres[0x06][0x05].Fila[1] = 0x00 ;
      EX4_Caracteres[0x06][0x05].Fila[2] = 0x06 ;
      EX4_Caracteres[0x06][0x05].Fila[3] = 0x09 ;
      EX4_Caracteres[0x06][0x05].Fila[4] = 0x0f ;
      EX4_Caracteres[0x06][0x05].Fila[5] = 0x08 ;
      EX4_Caracteres[0x06][0x05].Fila[6] = 0x07 ;
      // Digito f : -----------------------------------------------
      EX4_Caracteres[0x06][0x06].Fila[0] = 0x02 ;
      EX4_Caracteres[0x06][0x06].Fila[1] = 0x05 ;
      EX4_Caracteres[0x06][0x06].Fila[2] = 0x04 ;
      EX4_Caracteres[0x06][0x06].Fila[3] = 0x0e ;
      EX4_Caracteres[0x06][0x06].Fila[4] = 0x04 ;
      EX4_Caracteres[0x06][0x06].Fila[5] = 0x04 ;
      EX4_Caracteres[0x06][0x06].Fila[6] = 0x04 ;
      // Digito g : -----------------------------------------------
      EX4_Caracteres[0x06][0x07].Fila[0] = 0x00 ;
      EX4_Caracteres[0x06][0x07].Fila[1] = 0x00 ;
      EX4_Caracteres[0x06][0x07].Fila[2] = 0x07 ;
      EX4_Caracteres[0x06][0x07].Fila[3] = 0x09 ;
      EX4_Caracteres[0x06][0x07].Fila[4] = 0x07 ;
      EX4_Caracteres[0x06][0x07].Fila[5] = 0x01 ;
      EX4_Caracteres[0x06][0x07].Fila[6] = 0x0e ;
      // Digito h : -----------------------------------------------
      EX4_Caracteres[0x06][0x08].Fila[0] = 0x08 ;
      EX4_Caracteres[0x06][0x08].Fila[1] = 0x08 ;
      EX4_Caracteres[0x06][0x08].Fila[2] = 0x08 ;
      EX4_Caracteres[0x06][0x08].Fila[3] = 0x0a ;
      EX4_Caracteres[0x06][0x08].Fila[4] = 0x0d ;
      EX4_Caracteres[0x06][0x08].Fila[5] = 0x09 ;
      EX4_Caracteres[0x06][0x08].Fila[6] = 0x09 ;
      // Digito i : -----------------------------------------------
      EX4_Caracteres[0x06][0x09].Fila[0] = 0x00 ;
      EX4_Caracteres[0x06][0x09].Fila[1] = 0x04 ;
      EX4_Caracteres[0x06][0x09].Fila[2] = 0x00 ;
      EX4_Caracteres[0x06][0x09].Fila[3] = 0x0c ;
      EX4_Caracteres[0x06][0x09].Fila[4] = 0x04 ;
      EX4_Caracteres[0x06][0x09].Fila[5] = 0x04 ;
      EX4_Caracteres[0x06][0x09].Fila[6] = 0x0e ;
      // Digito j : -----------------------------------------------
      EX4_Caracteres[0x06][0x0a].Fila[0] = 0x02 ;
      EX4_Caracteres[0x06][0x0a].Fila[1] = 0x00 ;
      EX4_Caracteres[0x06][0x0a].Fila[2] = 0x06 ;
      EX4_Caracteres[0x06][0x0a].Fila[3] = 0x02 ;
      EX4_Caracteres[0x06][0x0a].Fila[4] = 0x02 ;
      EX4_Caracteres[0x06][0x0a].Fila[5] = 0x0a ;
      EX4_Caracteres[0x06][0x0a].Fila[6] = 0x04 ;
      // Digito k : -----------------------------------------------
      EX4_Caracteres[0x06][0x0b].Fila[0] = 0x08 ;
      EX4_Caracteres[0x06][0x0b].Fila[1] = 0x08 ;
      EX4_Caracteres[0x06][0x0b].Fila[2] = 0x09 ;
      EX4_Caracteres[0x06][0x0b].Fila[3] = 0x0a ;
      EX4_Caracteres[0x06][0x0b].Fila[4] = 0x0c ;
      EX4_Caracteres[0x06][0x0b].Fila[5] = 0x0a ;
      EX4_Caracteres[0x06][0x0b].Fila[6] = 0x09 ;
      // Digito l : -----------------------------------------------
      EX4_Caracteres[0x06][0x0c].Fila[0] = 0x0c ;
      EX4_Caracteres[0x06][0x0c].Fila[1] = 0x04 ;
      EX4_Caracteres[0x06][0x0c].Fila[2] = 0x04 ;
      EX4_Caracteres[0x06][0x0c].Fila[3] = 0x04 ;
      EX4_Caracteres[0x06][0x0c].Fila[4] = 0x04 ;
      EX4_Caracteres[0x06][0x0c].Fila[5] = 0x04 ;
      EX4_Caracteres[0x06][0x0c].Fila[6] = 0x0e ;
      // Digito m : -----------------------------------------------
      EX4_Caracteres[0x06][0x0d].Fila[0] = 0x00 ;
      EX4_Caracteres[0x06][0x0d].Fila[1] = 0x00 ;
      EX4_Caracteres[0x06][0x0d].Fila[2] = 0x1e ;
      EX4_Caracteres[0x06][0x0d].Fila[3] = 0x15 ;
      EX4_Caracteres[0x06][0x0d].Fila[4] = 0x15 ;
      EX4_Caracteres[0x06][0x0d].Fila[5] = 0x15 ;
      EX4_Caracteres[0x06][0x0d].Fila[6] = 0x15 ;
      // Digito n : -----------------------------------------------
      EX4_Caracteres[0x06][0x0e].Fila[0] = 0x00 ;
      EX4_Caracteres[0x06][0x0e].Fila[1] = 0x00 ;
      EX4_Caracteres[0x06][0x0e].Fila[2] = 0x0e ;
      EX4_Caracteres[0x06][0x0e].Fila[3] = 0x09 ;
      EX4_Caracteres[0x06][0x0e].Fila[4] = 0x09 ;
      EX4_Caracteres[0x06][0x0e].Fila[5] = 0x09 ;
      EX4_Caracteres[0x06][0x0e].Fila[6] = 0x09 ;
      // Digito o : -----------------------------------------------
      EX4_Caracteres[0x06][0x0f].Fila[0] = 0x00 ;
      EX4_Caracteres[0x06][0x0f].Fila[1] = 0x00 ;
      EX4_Caracteres[0x06][0x0f].Fila[2] = 0x06 ;
      EX4_Caracteres[0x06][0x0f].Fila[3] = 0x09 ;
      EX4_Caracteres[0x06][0x0f].Fila[4] = 0x09 ;
      EX4_Caracteres[0x06][0x0f].Fila[5] = 0x09 ;
      EX4_Caracteres[0x06][0x0f].Fila[6] = 0x06 ;
      // Digito p : -----------------------------------------------
      EX4_Caracteres[0x07][0x00].Fila[0] = 0x00 ;
      EX4_Caracteres[0x07][0x00].Fila[1] = 0x00 ;
      EX4_Caracteres[0x07][0x00].Fila[2] = 0x0e ;
      EX4_Caracteres[0x07][0x00].Fila[3] = 0x09 ;
      EX4_Caracteres[0x07][0x00].Fila[4] = 0x0e ;
      EX4_Caracteres[0x07][0x00].Fila[5] = 0x08 ;
      EX4_Caracteres[0x07][0x00].Fila[6] = 0x08 ;
      // Digito q : -----------------------------------------------
      EX4_Caracteres[0x07][0x01].Fila[0] = 0x00 ;
      EX4_Caracteres[0x07][0x01].Fila[1] = 0x00 ;
      EX4_Caracteres[0x07][0x01].Fila[2] = 0x05 ;
      EX4_Caracteres[0x07][0x01].Fila[3] = 0x0b ;
      EX4_Caracteres[0x07][0x01].Fila[4] = 0x09 ;
      EX4_Caracteres[0x07][0x01].Fila[5] = 0x07 ;
      EX4_Caracteres[0x07][0x01].Fila[6] = 0x01 ;
      // Digito r : -----------------------------------------------
      EX4_Caracteres[0x07][0x02].Fila[0] = 0x00 ;
      EX4_Caracteres[0x07][0x02].Fila[1] = 0x00 ;
      EX4_Caracteres[0x07][0x02].Fila[2] = 0x06 ;
      EX4_Caracteres[0x07][0x02].Fila[3] = 0x09 ;
      EX4_Caracteres[0x07][0x02].Fila[4] = 0x08 ;
      EX4_Caracteres[0x07][0x02].Fila[5] = 0x08 ;
      EX4_Caracteres[0x07][0x02].Fila[6] = 0x08 ;
      // Digito s : -----------------------------------------------
      EX4_Caracteres[0x07][0x03].Fila[0] = 0x00 ;
      EX4_Caracteres[0x07][0x03].Fila[1] = 0x00 ;
      EX4_Caracteres[0x07][0x03].Fila[2] = 0x07 ;
      EX4_Caracteres[0x07][0x03].Fila[3] = 0x08 ;
      EX4_Caracteres[0x07][0x03].Fila[4] = 0x06 ;
      EX4_Caracteres[0x07][0x03].Fila[5] = 0x01 ;
      EX4_Caracteres[0x07][0x03].Fila[6] = 0x0e ;
      // Digito t : -----------------------------------------------
      EX4_Caracteres[0x07][0x04].Fila[0] = 0x04 ;
      EX4_Caracteres[0x07][0x04].Fila[1] = 0x04 ;
      EX4_Caracteres[0x07][0x04].Fila[2] = 0x0e ;
      EX4_Caracteres[0x07][0x04].Fila[3] = 0x04 ;
      EX4_Caracteres[0x07][0x04].Fila[4] = 0x04 ;
      EX4_Caracteres[0x07][0x04].Fila[5] = 0x05 ;
      EX4_Caracteres[0x07][0x04].Fila[6] = 0x02 ;
      // Digito u : -----------------------------------------------
      EX4_Caracteres[0x07][0x05].Fila[0] = 0x00 ;
      EX4_Caracteres[0x07][0x05].Fila[1] = 0x00 ;
      EX4_Caracteres[0x07][0x05].Fila[2] = 0x09 ;
      EX4_Caracteres[0x07][0x05].Fila[3] = 0x09 ;
      EX4_Caracteres[0x07][0x05].Fila[4] = 0x09 ;
      EX4_Caracteres[0x07][0x05].Fila[5] = 0x09 ;
      EX4_Caracteres[0x07][0x05].Fila[6] = 0x07 ;
      // Digito v : -----------------------------------------------
      EX4_Caracteres[0x07][0x06].Fila[0] = 0x00 ;
      EX4_Caracteres[0x07][0x06].Fila[1] = 0x00 ;
      EX4_Caracteres[0x07][0x06].Fila[2] = 0x09 ;
      EX4_Caracteres[0x07][0x06].Fila[3] = 0x09 ;
      EX4_Caracteres[0x07][0x06].Fila[4] = 0x09 ;
      EX4_Caracteres[0x07][0x06].Fila[5] = 0x09 ;
      EX4_Caracteres[0x07][0x06].Fila[6] = 0x06 ;
      // Digito w : -----------------------------------------------
      EX4_Caracteres[0x07][0x07].Fila[0] = 0x00 ;
      EX4_Caracteres[0x07][0x07].Fila[1] = 0x00 ;
      EX4_Caracteres[0x07][0x07].Fila[2] = 0x11 ;
      EX4_Caracteres[0x07][0x07].Fila[3] = 0x11 ;
      EX4_Caracteres[0x07][0x07].Fila[4] = 0x15 ;
      EX4_Caracteres[0x07][0x07].Fila[5] = 0x15 ;
      EX4_Caracteres[0x07][0x07].Fila[6] = 0x0a ;
      // Digito x : -----------------------------------------------
      EX4_Caracteres[0x07][0x08].Fila[0] = 0x00 ;
      EX4_Caracteres[0x07][0x08].Fila[1] = 0x00 ;
      EX4_Caracteres[0x07][0x08].Fila[2] = 0x11 ;
      EX4_Caracteres[0x07][0x08].Fila[3] = 0x0a ;
      EX4_Caracteres[0x07][0x08].Fila[4] = 0x04 ;
      EX4_Caracteres[0x07][0x08].Fila[5] = 0x0a ;
      EX4_Caracteres[0x07][0x08].Fila[6] = 0x11 ;
      // Digito y : -----------------------------------------------
      EX4_Caracteres[0x07][0x09].Fila[0] = 0x00 ;
      EX4_Caracteres[0x07][0x09].Fila[1] = 0x00 ;
      EX4_Caracteres[0x07][0x09].Fila[2] = 0x09 ;
      EX4_Caracteres[0x07][0x09].Fila[3] = 0x09 ;
      EX4_Caracteres[0x07][0x09].Fila[4] = 0x07 ;
      EX4_Caracteres[0x07][0x09].Fila[5] = 0x01 ;
      EX4_Caracteres[0x07][0x09].Fila[6] = 0x06 ;
      // Digito z : -----------------------------------------------
      EX4_Caracteres[0x07][0x0a].Fila[0] = 0x00 ;
      EX4_Caracteres[0x07][0x0a].Fila[1] = 0x00 ;
      EX4_Caracteres[0x07][0x0a].Fila[2] = 0x0f ;
      EX4_Caracteres[0x07][0x0a].Fila[3] = 0x02 ;
      EX4_Caracteres[0x07][0x0a].Fila[4] = 0x04 ;
      EX4_Caracteres[0x07][0x0a].Fila[5] = 0x08 ;
      EX4_Caracteres[0x07][0x0a].Fila[6] = 0x0f ;
      // Digito 0 : -----------------------------------------------
      EX4_Caracteres[0x03][0x00].Fila[0] = 0x06 ;
      EX4_Caracteres[0x03][0x00].Fila[1] = 0x09 ;
      EX4_Caracteres[0x03][0x00].Fila[2] = 0x09 ;
      EX4_Caracteres[0x03][0x00].Fila[3] = 0x09 ;
      EX4_Caracteres[0x03][0x00].Fila[4] = 0x09 ;
      EX4_Caracteres[0x03][0x00].Fila[5] = 0x09 ;
      EX4_Caracteres[0x03][0x00].Fila[6] = 0x06 ;
      // Digito 1 : -----------------------------------------------
      EX4_Caracteres[0x03][0x01].Fila[0] = 0x02 ;
      EX4_Caracteres[0x03][0x01].Fila[1] = 0x06 ;
      EX4_Caracteres[0x03][0x01].Fila[2] = 0x02 ;
      EX4_Caracteres[0x03][0x01].Fila[3] = 0x02 ;
      EX4_Caracteres[0x03][0x01].Fila[4] = 0x02 ;
      EX4_Caracteres[0x03][0x01].Fila[5] = 0x02 ;
      EX4_Caracteres[0x03][0x01].Fila[6] = 0x07 ;
      // Digito 2 : -----------------------------------------------
      EX4_Caracteres[0x03][0x02].Fila[0] = 0x06 ;
      EX4_Caracteres[0x03][0x02].Fila[1] = 0x09 ;
      EX4_Caracteres[0x03][0x02].Fila[2] = 0x01 ;
      EX4_Caracteres[0x03][0x02].Fila[3] = 0x02 ;
      EX4_Caracteres[0x03][0x02].Fila[4] = 0x04 ;
      EX4_Caracteres[0x03][0x02].Fila[5] = 0x08 ;
      EX4_Caracteres[0x03][0x02].Fila[6] = 0x0f ;
      // Digito 3 : -----------------------------------------------
      EX4_Caracteres[0x03][0x03].Fila[0] = 0x06 ;
      EX4_Caracteres[0x03][0x03].Fila[1] = 0x09 ;
      EX4_Caracteres[0x03][0x03].Fila[2] = 0x01 ;
      EX4_Caracteres[0x03][0x03].Fila[3] = 0x02 ;
      EX4_Caracteres[0x03][0x03].Fila[4] = 0x01 ;
      EX4_Caracteres[0x03][0x03].Fila[5] = 0x09 ;
      EX4_Caracteres[0x03][0x03].Fila[6] = 0x06 ;
      // Digito 4 : -----------------------------------------------
      EX4_Caracteres[0x03][0x04].Fila[0] = 0x02 ;
      EX4_Caracteres[0x03][0x04].Fila[1] = 0x06 ;
      EX4_Caracteres[0x03][0x04].Fila[2] = 0x0a ;
      EX4_Caracteres[0x03][0x04].Fila[3] = 0x0f ;
      EX4_Caracteres[0x03][0x04].Fila[4] = 0x02 ;
      EX4_Caracteres[0x03][0x04].Fila[5] = 0x02 ;
      EX4_Caracteres[0x03][0x04].Fila[6] = 0x02 ;
      // Digito 5 : -----------------------------------------------
      EX4_Caracteres[0x03][0x05].Fila[0] = 0x0f ;
      EX4_Caracteres[0x03][0x05].Fila[1] = 0x08 ;
      EX4_Caracteres[0x03][0x05].Fila[2] = 0x0e ;
      EX4_Caracteres[0x03][0x05].Fila[3] = 0x01 ;
      EX4_Caracteres[0x03][0x05].Fila[4] = 0x01 ;
      EX4_Caracteres[0x03][0x05].Fila[5] = 0x09 ;
      EX4_Caracteres[0x03][0x05].Fila[6] = 0x06 ;
      // Digito 6 : -----------------------------------------------
      EX4_Caracteres[0x03][0x06].Fila[0] = 0x03 ;
      EX4_Caracteres[0x03][0x06].Fila[1] = 0x04 ;
      EX4_Caracteres[0x03][0x06].Fila[2] = 0x08 ;
      EX4_Caracteres[0x03][0x06].Fila[3] = 0x0e ;
      EX4_Caracteres[0x03][0x06].Fila[4] = 0x09 ;
      EX4_Caracteres[0x03][0x06].Fila[5] = 0x09 ;
      EX4_Caracteres[0x03][0x06].Fila[6] = 0x06 ;
      // Digito 7 : -----------------------------------------------
      EX4_Caracteres[0x03][0x07].Fila[0] = 0x0f ;
      EX4_Caracteres[0x03][0x07].Fila[1] = 0x01 ;
      EX4_Caracteres[0x03][0x07].Fila[2] = 0x02 ;
      EX4_Caracteres[0x03][0x07].Fila[3] = 0x04 ;
      EX4_Caracteres[0x03][0x07].Fila[4] = 0x08 ;
      EX4_Caracteres[0x03][0x07].Fila[5] = 0x08 ;
      EX4_Caracteres[0x03][0x07].Fila[6] = 0x08 ;
      // Digito 8 : -----------------------------------------------
      EX4_Caracteres[0x03][0x08].Fila[0] = 0x06 ;
      EX4_Caracteres[0x03][0x08].Fila[1] = 0x09 ;
      EX4_Caracteres[0x03][0x08].Fila[2] = 0x09 ;
      EX4_Caracteres[0x03][0x08].Fila[3] = 0x06 ;
      EX4_Caracteres[0x03][0x08].Fila[4] = 0x09 ;
      EX4_Caracteres[0x03][0x08].Fila[5] = 0x09 ;
      EX4_Caracteres[0x03][0x08].Fila[6] = 0x06 ;
      // Digito 9 : -----------------------------------------------
      EX4_Caracteres[0x03][0x09].Fila[0] = 0x06 ;
      EX4_Caracteres[0x03][0x09].Fila[1] = 0x09 ;
      EX4_Caracteres[0x03][0x09].Fila[2] = 0x09 ;
      EX4_Caracteres[0x03][0x09].Fila[3] = 0x07 ;
      EX4_Caracteres[0x03][0x09].Fila[4] = 0x01 ;
      EX4_Caracteres[0x03][0x09].Fila[5] = 0x02 ;
      EX4_Caracteres[0x03][0x09].Fila[6] = 0x0c ;
      // Digito : : -----------------------------------------------
      EX4_Caracteres[0x03][0x0a].Fila[0] = 0x00 ;
      EX4_Caracteres[0x03][0x0a].Fila[1] = 0x06 ;
      EX4_Caracteres[0x03][0x0a].Fila[2] = 0x06 ;
      EX4_Caracteres[0x03][0x0a].Fila[3] = 0x00 ;
      EX4_Caracteres[0x03][0x0a].Fila[4] = 0x06 ;
      EX4_Caracteres[0x03][0x0a].Fila[5] = 0x06 ;
      EX4_Caracteres[0x03][0x0a].Fila[6] = 0x00 ;
      // Digito ; : -----------------------------------------------
      EX4_Caracteres[0x03][0x0b].Fila[0] = 0x00 ;
      EX4_Caracteres[0x03][0x0b].Fila[1] = 0x00 ;
      EX4_Caracteres[0x03][0x0b].Fila[2] = 0x06 ;
      EX4_Caracteres[0x03][0x0b].Fila[3] = 0x00 ;
      EX4_Caracteres[0x03][0x0b].Fila[4] = 0x06 ;
      EX4_Caracteres[0x03][0x0b].Fila[5] = 0x02 ;
      EX4_Caracteres[0x03][0x0b].Fila[6] = 0x04 ;
      // Digito < : -----------------------------------------------
      EX4_Caracteres[0x03][0x0c].Fila[0] = 0x00 ;
      EX4_Caracteres[0x03][0x0c].Fila[1] = 0x00 ;
      EX4_Caracteres[0x03][0x0c].Fila[2] = 0x01 ;
      EX4_Caracteres[0x03][0x0c].Fila[3] = 0x02 ;
      EX4_Caracteres[0x03][0x0c].Fila[4] = 0x04 ;
      EX4_Caracteres[0x03][0x0c].Fila[5] = 0x02 ;
      EX4_Caracteres[0x03][0x0c].Fila[6] = 0x01 ;
      // Digito > : -----------------------------------------------
      EX4_Caracteres[0x03][0x0e].Fila[0] = 0x00 ;
      EX4_Caracteres[0x03][0x0e].Fila[1] = 0x00 ;
      EX4_Caracteres[0x03][0x0e].Fila[2] = 0x04 ;
      EX4_Caracteres[0x03][0x0e].Fila[3] = 0x02 ;
      EX4_Caracteres[0x03][0x0e].Fila[4] = 0x01 ;
      EX4_Caracteres[0x03][0x0e].Fila[5] = 0x02 ;
      EX4_Caracteres[0x03][0x0e].Fila[6] = 0x04 ;
      // Digito = : -----------------------------------------------
      EX4_Caracteres[0x03][0x0d].Fila[0] = 0x00 ;
      EX4_Caracteres[0x03][0x0d].Fila[1] = 0x00 ;
      EX4_Caracteres[0x03][0x0d].Fila[2] = 0x00 ;
      EX4_Caracteres[0x03][0x0d].Fila[3] = 0x0f ;
      EX4_Caracteres[0x03][0x0d].Fila[4] = 0x00 ;
      EX4_Caracteres[0x03][0x0d].Fila[5] = 0x0f ;
      EX4_Caracteres[0x03][0x0d].Fila[6] = 0x00 ;
      // Digito ? : -----------------------------------------------
      EX4_Caracteres[0x03][0x0f].Fila[0] = 0x06 ;
      EX4_Caracteres[0x03][0x0f].Fila[1] = 0x09 ;
      EX4_Caracteres[0x03][0x0f].Fila[2] = 0x01 ;
      EX4_Caracteres[0x03][0x0f].Fila[3] = 0x02 ;
      EX4_Caracteres[0x03][0x0f].Fila[4] = 0x04 ;
      EX4_Caracteres[0x03][0x0f].Fila[5] = 0x00 ;
      EX4_Caracteres[0x03][0x0f].Fila[6] = 0x04 ;
      // Digito } : -----------------------------------------------
      EX4_Caracteres[0x07][0x0d].Fila[0] = 0x04 ;
      EX4_Caracteres[0x07][0x0d].Fila[1] = 0x02 ;
      EX4_Caracteres[0x07][0x0d].Fila[2] = 0x02 ;
      EX4_Caracteres[0x07][0x0d].Fila[3] = 0x01 ;
      EX4_Caracteres[0x07][0x0d].Fila[4] = 0x02 ;
      EX4_Caracteres[0x07][0x0d].Fila[5] = 0x02 ;
      EX4_Caracteres[0x07][0x0d].Fila[6] = 0x04 ;
      // Digito { : -----------------------------------------------
      EX4_Caracteres[0x07][0x0b].Fila[0] = 0x01 ;
      EX4_Caracteres[0x07][0x0b].Fila[1] = 0x02 ;
      EX4_Caracteres[0x07][0x0b].Fila[2] = 0x02 ;
      EX4_Caracteres[0x07][0x0b].Fila[3] = 0x04 ;
      EX4_Caracteres[0x07][0x0b].Fila[4] = 0x02 ;
      EX4_Caracteres[0x07][0x0b].Fila[5] = 0x02 ;
      EX4_Caracteres[0x07][0x0b].Fila[6] = 0x01 ;
      // Digito ¿ : -----------------------------------------------
      EX4_Caracteres[0x0b][0x0f].Fila[0] = 0x02 ;
      EX4_Caracteres[0x0b][0x0f].Fila[1] = 0x00 ;
      EX4_Caracteres[0x0b][0x0f].Fila[2] = 0x02 ;
      EX4_Caracteres[0x0b][0x0f].Fila[3] = 0x04 ;
      EX4_Caracteres[0x0b][0x0f].Fila[4] = 0x08 ;
      EX4_Caracteres[0x0b][0x0f].Fila[5] = 0x09 ;
      EX4_Caracteres[0x0b][0x0f].Fila[6] = 0x06 ;

}
