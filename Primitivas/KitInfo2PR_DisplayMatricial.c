/**
 	\file KitInfo2PR_DisplayMatricial.c
 	\brief Primitiva del Display Matricial
 	\details Expansion 4
 	\author Ing. Marcelo Trujillo
 	\date 2012.04.25
*/
#include "KitInfo2.h"

extern struct matriz EX4_Caracteres[16][16];
extern volatile struct matriz EX4_BufferDisplay[];
extern volatile struct matriz EX4_BufferMatriz[];
extern volatile int EX4_lstring;
extern volatile unsigned char EX4_modo;
/**	
	\fn  int EX4_Display( char * , char  )
	\brief Carga de los buffers del display matricial
  	\author Ing. Marcelo Trujillo
 	\date 2011.12.03
 	\param [in] msg mensaje a mostrar
	\param [in] modo de presentacion
	<ul>
		<li> Modo de funcionamiento
		<ol> 
		<li> ROTAR_DERECHA : Rotar a la derecha	
		<li> ROTAR_IZQUIERDA : Rotar a la izquierda	
		<li> ROTAR_ARRIBA : Rotar hacia arriba
		<li> ROTAR_ABAJO : Rotar hacia abajo	
		</ol>
	</ul>	
	\return Retorna 1 por error y o por �xito
*/

int EX4_Display( char *msg , char Modo )
{
	int i ,j;

	if ( msg )
	{
		if ( ( EX4_lstring = strlen ( msg ) )  > EX4_BUFFER )
			return 1 ;

		for ( i = 0 ; i < EX4_lstring ; i++ )
			EX4_BufferDisplay[ i ] = EX4_Caracteres[ ( msg[ i ] >> 4 ) & 0x0f ][ msg[ i ] & 0x0f ] ;	
		
		for ( i = 0 ; i < EX4_DIGITOS  ; i++ )
			EX4_BufferMatriz[ i ] = EX4_BufferDisplay[ EX4_DIGITOS - i - 1 ] ;
		
	}
	else
	{
		for (j = 0 ; j <  EX4_lstring ; j ++ )
		{
			for ( i = 0 ; i < 7 ; i ++)
				EX4_BufferDisplay[ j ].Fila[ i ] &= 0x3f;
		}
	}

	EX4_modo = Modo ;
	return 0;
}

