/*
 * KitInfo2PR_serie.c
 *
 *  Created on: 23/03/2014
 *      Author: Papuepes
 */
#include "KitInfo2.h"

extern volatile uint8_t TxStart;
// Buffer de Transmision
extern volatile uint8_t BufferTx[TXBUFFER_SIZE];
// Buffer de Recepcion
extern volatile uint8_t BufferRx[RXBUFFER_SIZE];

// Indices de Transmision
extern volatile uint8_t IndiceTxIn,IndiceTxOut;

// Indices de Recepcion
extern volatile uint8_t IndiceRxIn,IndiceRxOut;

extern volatile uint8_t UPrg[];
extern volatile int UPrgAuxiliar[];

extern char ValidarTrama[];
extern uint8_t CantidadCaracteresTrama;
extern volatile uint8_t TxStart;

extern volatile uint8_t BufferEntradas;
extern volatile uint8_t BufferSalidas;				//!< Buffer de Salidas Relay
extern uint32_t valorADC[ ];

void PushTx( uint8_t dato)
{
	BufferTx[IndiceTxIn] = dato;

	IndiceTxIn ++;
	IndiceTxIn %= TXBUFFER_SIZE;

	if ( TxStart == 0 )
	{
		TxStart = 1;
		START_TX;
	}
}

int PopTx( void )
{
	int dato = NO_KEY;

	if ( IndiceTxIn != IndiceTxOut )
	{
		dato = ( uint32_t ) BufferTx[IndiceTxOut];
		IndiceTxOut ++;
		IndiceTxOut %= TXBUFFER_SIZE;
	}
	return dato;
}

void PushRx( uint8_t dato )
{
	BufferRx[IndiceRxIn] = dato;
	IndiceRxIn ++;
	IndiceRxIn %= RXBUFFER_SIZE;
}

int PopRx( void )
{

	int dato = NO_KEY;

	if ( IndiceRxIn != IndiceRxOut )
	{
		dato = (int32_t) BufferRx[IndiceRxOut];
		IndiceRxOut ++;
		IndiceRxOut %= RXBUFFER_SIZE;
	}
	return dato;
}
void EnviarString (const char *str)
{
	uint32_t i;
	for( i = 0 ; str[i] ; i++ )
		PushTx( str[i] );
}

/**************************************************************************/
/*	Proposito: Transmite Valores de Proceso.          		 		*/
/*	Parametros: void.						  										*/
/*	Retorna: void							  										*/
/**************************************************************************/
void TX_Prg(void)
{
	uint8_t a;

	for( a = 0; a < TOPE; a++)
		if( a < 4 )
			UPrg[a] = UPrgAuxiliar[a];
		else
			UPrg[11 - a] = ( UPrgAuxiliar[ 4 ] >> ( a - 4 ) * 8 ) & 0xFF;

	PushTx('p');

	for(a = 0;a <= TOPE; a++)
		PushTx(UPrg[a]);

}

/**************************************************************************/
/*	Proposito: Transmitir Tecla.                               		 		*/
/*	Parametros: void.						  										*/
/*	Retorna: void							  										*/
/**************************************************************************/
void TX_Key(unsigned char c)
{
	//PushRx('k');
	//PushRx(c);
	PushTx('k');
	PushTx(c);
}

uint8_t LeerBufferRX( void )
{
	uint8_t caracter;

	caracter = PopRx();

		if ( caracter == NO_KEY )
			return NO_KEY;

	return caracter;
}

void AnalizarTramaUart1(void)
{
	uint8_t caracter, enviarUart1 = 0;
	static uint8_t i = 0;
	volatile static uint8_t estado = NO_KEY;
	static uint8_t relay, entrada;
	unsigned int primerDigitoMiliVolt = 0, decimalesMiliVolts = 0, aux;
	char texto[40];

	if( !CantidadCaracteresTrama )
	{
		caracter = LeerBufferRX();
		if( caracter != NO_KEY)

		{
			sprintf( texto, "Dato en UART1: %c", caracter) ;
			Display_lcd( texto, 0 , 0 );
			enviarUart1 = 1;

		}

		if ( enviarUart1 == 1 )
		{

			PushTx( caracter);
			enviarUart1 = 0;
		}
	}
	else
	{


		caracter = LeerBufferRX();

		if( caracter != NO_KEY)
		{

			sprintf( texto, "Dato en UART1: %c", caracter) ;
			Display_lcd( texto, 0 , 0 );
			if( caracter == ValidarTrama[i] )
			{
				i++;
				TxStart = 0;
				PushTx( caracter);
				if( i == CantidadCaracteresTrama )
				{
					sprintf( texto, "    TRAMA OK    ") ;
					Display_lcd( texto, 0 , 0 );
					EnviarString("\n\r-- TRAMA OK  --\n\r");
				}
				i %= CantidadCaracteresTrama;

			}
			else
			{
				caracter = toupper(caracter);		//Pasa a mayúsculas solo letras
				switch (caracter)
				{
				case 'R':
						if( estado != 3 )		//Choca con la R de read
							estado = 0;
						break;
				case 'E':
						estado = 6;
						break;

				default:
						i = 0;
						//estado = NO_KEY;
						break;
				}

			}

			switch(estado)
			{
			case 0:
				if( caracter == 'R' )
					estado = 1;
				else
					estado = NO_KEY;

				break;
			case 1:
					if( caracter == 'L' )
						estado = 2;
					else
						estado = NO_KEY;

					break;
			case 2:
					if( caracter == '0' || caracter == '1' || caracter == '2' || caracter == '3' )
					{
						estado = 3;
						relay = caracter - '0';
					}
					else
						estado = NO_KEY;


					break;
			case 3:
				 if( caracter == 'W' )
					estado = 4;
				 else
					{
					 	 if( caracter == 'R' )
							{
							if( ( BufferSalidas >> relay ) & 0x01 )
								sprintf( texto, "\n\rRELAY %d ON\n\r",relay);
							else
								sprintf( texto, "\n\rRELAY %d OFF\n\r",relay);
							Display_lcd( texto, 0 , 0 );
							EnviarString(texto);
							estado = NO_KEY;
							}
						else
							estado = NO_KEY;
					}



				break;
			case 4:
					if( caracter == '1' )
					{
						Relays (relay, ON);
						sprintf( texto, "\n\rRELAY %d ON\n\r",relay) ;
					}
					if( caracter == '0')
					{
						Relays (relay, OFF);
						sprintf( texto, "\n\rRELAY %d OFF\n\r",relay) ;
					}

					Display_lcd( texto, 0 , 0 );
					EnviarString(texto);
					estado = NO_KEY;

					break;
			case 5:

					break;
			case 6:
				if( caracter == 'E' )
					estado = 7;
				else
					estado = NO_KEY;

				break;
			case 7:
				if( caracter == 'D' )
					estado = 8;
				else
					{
					if( caracter == 'A' )

						estado = 9;
					else
						estado = NO_KEY;
					}
				break;
			case 8:
					if( caracter == '0' || caracter == '1' || caracter == '2'  )
						{
						entrada = caracter - '0';
						if( ( BufferEntradas >> entrada ) & 0x01 )
							sprintf( texto, "\n\rENTRADA DIGITAL %d OFF\n\r",entrada); //Lógica negativa
						else
							sprintf( texto, "\n\rENTRADA DIGITAL %d ON\n\r",entrada);  //Lógica negativa
						Display_lcd( texto, 0 , 0 );
						EnviarString(texto);
						estado = NO_KEY;

						}
					else
						estado = NO_KEY;
					break;
			case 9:
					if( caracter == '0' || caracter == '1' || caracter == '2'  )
					{
						entrada = caracter - '0';
						aux = ConvertirMiliVolts( valorADC[entrada] );
						primerDigitoMiliVolt = aux / 1000;
						aux %=  1000;
						decimalesMiliVolts = aux;
						sprintf( texto, "\n\rENTRADA ANALOGICA %d: %d.%d Volts\n\r", entrada, primerDigitoMiliVolt, decimalesMiliVolts) ;
						Display_lcd( texto, 0 , 0 );
						EnviarString(texto);
						estado = NO_KEY;
						break;
					}
					else
						estado = NO_KEY;
					break;

			default:
					break;
			}

			}
		}

	}


int8_t toupper(int8_t caracter)
{
	if( caracter >= 0x61 && caracter <= 0x7A)	//a=0x61, z=0x7A
		caracter -= 0x20;						//A=0x41, Z=0x5A
	return caracter;
}







