/**
 	\file KitInfo2PR_I2C.c
 	\brief Primitiva de teclado
 	\details Valida para BASE y Expansion 3 y Expansion 4
 	\author Ing. Marcelo Trujillo
 	\date 2012.04.25
*/
#include <KitInfo2.h>

extern volatile uint32_t I2CMasterState;
extern volatile uint32_t I2CSlaveState;

extern volatile uint32_t I2CCmd;
extern volatile uint32_t I2CMode;

extern volatile uint8_t I2CMasterBuffer[];
extern volatile uint8_t I2CSlaveBuffer[];
extern volatile uint32_t I2CCount;
extern volatile uint32_t I2CReadLength;
extern volatile uint32_t I2CWriteLength;

extern volatile uint32_t RdIndex;
extern volatile uint32_t WrIndex;


/********************************************************************************************/
uint8_t  EX_EEPROM_WRITE(uint16_t adres, uint8_t veri)
{
  uint8_t i;
  for ( i = 0; i < BUFSIZE; i++ )	/* clear buffer */
  {
	I2CMasterBuffer[i] = 0;
  }
  I2CWriteLength = 4;
  I2CReadLength  = 0;
  I2CMasterBuffer[0] = ADR_BYTE;
  I2CMasterBuffer[1] = (adres >> 8) & 0xff;
  I2CMasterBuffer[2] = adres & 0xff;
  I2CMasterBuffer[3] = veri;
  if(I2CEngine())
	  return 1;
  else
	  return 0;
}
/********************************************************************************************/
uint8_t  EX_EEPROM_READ(uint16_t adres)
{
	uint8_t i;
	for ( i = 0; i < BUFSIZE; i++ )	  	/* clear buffer */
  {
	I2CMasterBuffer[i] = 0;
  }
  I2CWriteLength = 3;
  I2CReadLength = 1;
  I2CMasterBuffer[0] = ADR_BYTE;
  //I2CMasterBuffer[0] = ADR_BYTE | RD_BIT;
  I2CMasterBuffer[1] = ( adres >> 8 ) & 0xff;
  I2CMasterBuffer[2] = adres & 0xff;
  I2CMasterBuffer[3] = ADR_BYTE | RD_BIT;
  I2CEngine();
  I2CStop();
  return (I2CMasterBuffer[4]);

}
