/**
 	\file KitInfo2PR_Teclado.c
 	\brief Primitiva de teclado
 	\details Valida para BASE y Expansion 3 y Expansion 4
 	\author Ing. Marcelo Trujillo
 	\date 2012.04.25
*/
#include "KitInfo2.h"

// Buffer de teclado
extern volatile unsigned char key;
extern volatile unsigned char keyFija;

unsigned char Teclado( void )
{
	unsigned char Key = NO_KEY;

	if (key != NO_KEY )
	{
		Key = key;
		key = NO_KEY;
	}
	TimerEvent();

	return Key;
}

unsigned char TecladoFijo( void )
{
	unsigned char Key = NO_KEY;

	if (keyFija != NO_KEY )
	{
		Key = keyFija;
		keyFija = NO_KEY;
	}

	return Key;
}
